//
//  XQSettingsTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/4/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQSettingsTableViewController.h"
#import "XQExquisenseSecurity.h"
#import "XQAboutViewController.h"
#import "XQAppDelegate.h"
#import "XQImportDatabaseTableViewController.h"

#define TO_ABOUT_VIEW_CONTROLLER @"toAboutViewController"
#define TO_IMPORT_DATABASE_TABLE_VIEW_CONTROLLER @"toImportDatabaseTableViewController"
#define TO_CONFIGURE_NETWORK_TABLE_VIEW_CONTROLLER @"toConfigureNetworkTableViewController"

@interface XQSettingsTableViewController () <UITextFieldDelegate>
{
    XQAppDelegate *appDelegate;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@property (strong, nonatomic) IBOutlet UILabel *connectionStatusLabel;

@property (strong, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;

- (IBAction)aboutBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)saveBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)requiredFieldFilled :(id)sender;
- (IBAction)importDataButtonPressed:(UIButton *)sender;
- (IBAction)exportDataButtonPressed:(UIButton *)sender;

@end

@implementation XQSettingsTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    [self.oldPasswordTextField setDelegate:self];
    [self.passwordTextField setDelegate:self];
    [self.confirmPasswordTextField setDelegate:self];
    [self.saveButton setEnabled:NO];
    [self.connectionStatusLabel setText:@""];
    
    [self.oldPasswordTextField setReturnKeyType:UIReturnKeyNext];
    [self.passwordTextField setReturnKeyType:UIReturnKeyNext];
    [self.confirmPasswordTextField setReturnKeyType:UIReturnKeyDone];
    
    [XQExquisense addSwipeDownGestureRecognizerToViewController:self
                                                         action:@selector(swipeDownGestureRecognized)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *pairedDevice = [[appDelegate client] getPairedDeviceName];
    if (pairedDevice)
    {
        [self.connectionStatusLabel setTextColor:[UIColor blueColor]];
        [self.connectionStatusLabel setText:[NSString stringWithFormat: @"Connected to %@", pairedDevice]];
    }
    else
    {
        [self.connectionStatusLabel setTextColor:[UIColor redColor]];
        [self.connectionStatusLabel setText:@"Not connected"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)aboutBarButtonPressed:(UIBarButtonItem *)sender
{
    [self performSegueWithIdentifier:TO_ABOUT_VIEW_CONTROLLER sender:self];
}

- (IBAction)saveBarButtonPressed:(UIBarButtonItem *)sender
{
    NSString *hashedPassword = [XQExquisenseSecurity SHA512OfString:self.oldPasswordTextField.text];
    NSString *masterCredentials = (NSString *) [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
    
    if (![hashedPassword isEqualToString:masterCredentials])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Password"
                                                        message:@"Please make sure to type the password correctly."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Mismatch"
                                                        message:@"Please confirm your password."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([hashedPassword isEqualToString:masterCredentials] &&
             [self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        NSString *hashedNewPassword = [XQExquisenseSecurity SHA512OfString:self.passwordTextField.text];
        
        [[NSUserDefaults standardUserDefaults] setObject:hashedNewPassword forKey:MASTER_CREDENTIALS_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Change Successful"
                                                        message:@"You have successfully changed your password."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [appDelegate saveDatabaseWithPassword:hashedNewPassword];
    }
    
    [self.oldPasswordTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
}

- (IBAction)requiredFieldFilled :(id)sender
{
    if (!([self.oldPasswordTextField.text isEqualToString:@""] ||
          [self.passwordTextField.text isEqualToString:@""] ||
          [self.confirmPasswordTextField.text isEqualToString:@""]))
    {
        [self.saveButton setEnabled:YES];
    }
    else
    {
        [self.saveButton setEnabled:NO];
    }
}

- (IBAction)importDataButtonPressed:(UIButton *)sender
{
    [self performSegueWithIdentifier:TO_IMPORT_DATABASE_TABLE_VIEW_CONTROLLER sender:self];
}

- (IBAction)exportDataButtonPressed:(UIButton *)sender
{
    NSString *documentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *date = [dateFormatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@ %@.%@", @"LOQ Backup", date, @"loq"];
    NSString *path = [documentsPath stringByAppendingPathComponent:fileName];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_KEY];
    
    BOOL success = [data writeToFile:path atomically:YES];
    
    if (ENABLE_LOGGING) NSLog(@"%@ exporting data to %@", success ? @"Success" : @"Failed", path);
}

#pragma mark - Selector Methods

- (void)swipeDownGestureRecognized
{
    [self.oldPasswordTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.oldPasswordTextField])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.passwordTextField])
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.confirmPasswordTextField])
    {
        [self.confirmPasswordTextField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            [self performSegueWithIdentifier:TO_CONFIGURE_NETWORK_TABLE_VIEW_CONTROLLER sender:self];
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            [self.oldPasswordTextField becomeFirstResponder];
        }
        else if (indexPath.row == 1)
        {
            [self.passwordTextField becomeFirstResponder];
        }
        else if (indexPath.row == 2)
        {
            [self.confirmPasswordTextField becomeFirstResponder];
        }
    }
}

@end
