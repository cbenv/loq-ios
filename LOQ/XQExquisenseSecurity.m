//
//  XQExquisenseSecurity.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/6/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseSecurity.h"
#import <CommonCrypto/CommonDigest.h>
#import "RNEncryptor.h"
#import "RNDecryptor.h"

@implementation XQExquisenseSecurity

#pragma mark - Hash

+ (NSString *)SHAOfString:(NSString *)string withDigestLength:(NSInteger)digestLength
{
    const char *c = [string cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:c length:string.length];
    uint8_t digest[digestLength];
    
    switch (digestLength)
    {
        case CC_SHA1_DIGEST_LENGTH:
            CC_SHA1(data.bytes, data.length, digest);
            break;
        case CC_SHA256_DIGEST_LENGTH:
            CC_SHA256(data.bytes, data.length, digest);
            break;
        case CC_SHA512_DIGEST_LENGTH:
            CC_SHA512(data.bytes, data.length, digest);
            break;
        default:
            CC_SHA512(data.bytes, data.length, digest);
            break;
    }
    
    NSMutableString *hash = [NSMutableString stringWithCapacity:digestLength * 2];
    
    for (int i = 0; i < digestLength; i++)
    {
        [hash appendFormat:@"%02x", digest[i]];
    }
    
    return hash;
}

+ (NSString *)SHA1OfString:(NSString *)string
{
    return [[self class] SHAOfString:string withDigestLength:CC_SHA1_DIGEST_LENGTH];
}

+ (NSString *)SHA256OfString:(NSString *)string
{
    return [[self class] SHAOfString:string withDigestLength:CC_SHA256_DIGEST_LENGTH];
}

+ (NSString *)SHA512OfString:(NSString *)string
{
    return [[self class] SHAOfString:string withDigestLength:CC_SHA512_DIGEST_LENGTH];
}

#pragma mark - Generate Password

+ (NSString *)generatePassword
{
    NSDictionary *passwordGeneratorSettings = [[NSUserDefaults standardUserDefaults] dictionaryForKey:PASSWORD_GENERATOR_SETTINGS_KEY];
    
    int characterCount = [[passwordGeneratorSettings valueForKey:PASSWORD_GENERATOR_PASSWORD_LENGTH] intValue];
    BOOL useLowercaseCharacters = [[passwordGeneratorSettings valueForKey:PASSWORD_GENERATOR_USE_LOWERCASE_ALPHABETS] boolValue];
    BOOL useUppercaseCharacters = [[passwordGeneratorSettings valueForKey:PASSWORD_GENERATOR_USE_UPPERCASE_ALPHABETS] boolValue];
    BOOL useNumbers = [[passwordGeneratorSettings valueForKey:PASSWORD_GENERATOR_USE_NUMBERS] boolValue];
    BOOL useSymbols = [[passwordGeneratorSettings valueForKey:PASSWORD_GENERATOR_USE_SPECIAL_CHARACTERS] boolValue];
    BOOL useAmbiguousCharacters = [[passwordGeneratorSettings valueForKey:PASSWORD_GENERATOR_USE_AMBIGUOUS_CHARACTERS] boolValue];
    
    NSMutableString *availableChars = [[NSMutableString alloc] init];
    NSMutableString *password = [[NSMutableString alloc] init];
    
    if (useLowercaseCharacters)
    {
        [availableChars appendString:@"abcdefghijkmnopqrstuvwxyz"];
    }
    if (useUppercaseCharacters)
    {
        [availableChars appendString:@"ABCDEFGHJKLMNPQRSTUVWXYZ"];
    }
    if (useNumbers)
    {
        [availableChars appendString:@"23456789"];
    }
    if (useSymbols)
    {
        [availableChars appendString:@"~`!@#$%^&*()-_+={}[]|\\:;\"\'<>,.?/"];
    }
    if (useAmbiguousCharacters)
    {
        [availableChars appendString:@"1lIO0"];
    }
    
    for (int i = 0; i < (int) characterCount; i++)
    {
        int r = arc4random_uniform([availableChars length]);
        char c = [availableChars characterAtIndex:r];
        [password appendFormat:@"%c", c];
    }
    
    return password;
}

#pragma mark - Encrypt and Decrypt

+ (NSData *)AESEncryptOfData:(NSData *)data withKey:(NSString *)key
{
    return [RNEncryptor encryptData:data
                       withSettings:kRNCryptorAES256Settings
                           password:key
                              error:nil];
}

+ (NSData *)AESDecryptOfData:(NSData *)data withKey:(NSString *)key
{
    return [RNDecryptor decryptData:data
                       withPassword:key
                              error:nil];
}

@end
