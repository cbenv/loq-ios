//
//  XQTokenCode.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/10/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//
//  Adapted from FreeOTP by Nathaniel McCallum <npmccallum@redhat.com> with several modifications.
//  In compliance with Apache License v2.0, as required by licensor.
//  Copyright (C) 2013 Nathaniel McCallum, RedHat.
//

#import <Foundation/Foundation.h>
#import <sys/time.h>

@interface XQTokenCode : NSObject

- (id)initWithCode:(NSString*)code startTime:(time_t)start endTime:(time_t)end;
- (id)initWithCode:(NSString*)code startTime:(time_t)start endTime:(time_t)end nextTokenCode:(XQTokenCode *)next;

- (NSString *)currentCode;
- (float)currentProgress;
- (float)totalProgress;
- (NSUInteger)totalCodes;

@end
