//
//  XQAppDelegate.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/3/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XQExquisenseDatabase.h"
#import "XQExquisenseBonjourClient.h"

@interface XQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (UIStatusBarStyle)preferredStatusBarStyle;

#pragma mark - Database

@property (strong, nonatomic) XQExquisenseDatabase *database;

- (void)saveDatabaseWithPassword:(NSString *)password;
- (void)loadDatabaseWithPassword:(NSString *)password;

#pragma mark - Bonjour

@property (strong, nonatomic) XQExquisenseBonjourClient *client;

@end
