//
//  XQTokenCode.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/10/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//
//  Adapted from FreeOTP by Nathaniel McCallum <npmccallum@redhat.com> with several modifications.
//  In compliance with Apache License v2.0, as required by licensor.
//  Copyright (C) 2013 Nathaniel McCallum, RedHat.
//

#import "XQTokenCode.h"

#pragma mark - Static Methods

static uint64_t currentTimeInMilli()
{
    struct timeval tv;
    
    if (gettimeofday(&tv, NULL) != 0)
    {
        return 0;
    }
    
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

@implementation XQTokenCode
{
    XQTokenCode *nextCode;
    NSString* codeText;
    uint64_t startTime;
    uint64_t endTime;
}

- (id)initWithCode:(NSString*)code startTime:(time_t)start endTime:(time_t)end
{
    codeText = code;
    startTime = start * 1000;
    endTime = end * 1000;
    nextCode = nil;
    return self;
}

- (id)initWithCode:(NSString*)code startTime:(time_t)start endTime:(time_t)end nextTokenCode:(XQTokenCode *)next
{
    self = [self initWithCode:code startTime:start endTime:end];
    nextCode = next;
    return self;
}

- (NSString*)currentCode
{
    uint64_t now = currentTimeInMilli();
        
    if (now < startTime)
    {
        return nil;
    }
    
    if (now < endTime)
    {
        return codeText;
    }
    
    if (nextCode != nil)
    {
        return [nextCode currentCode];
    }
    
    return nil;
}

- (float)currentProgress
{
    uint64_t now = currentTimeInMilli();
    
    if (now < startTime)
    {
        return 0.0;
    }
    
    if (now < endTime)
    {
        float totalTime = (float) (endTime - startTime);
        return (now - startTime) / totalTime;
    }
    
    if (nextCode != nil)
    {
        return [nextCode currentProgress];
    }
    
    return 0.0;
}

- (float)totalProgress
{
    uint64_t now = currentTimeInMilli();
    XQTokenCode* last = self;
    
    if (now < startTime)
    {
        return 0.0;
    }
    
    // Find the last token code.
    while (last->nextCode != nil)
    {
        last = last->nextCode;
    }
    
    if (now < last->endTime)
    {
        float totalTime = (float) (last->endTime - startTime);
        return (now - startTime) / totalTime;
    }
    
    return 0.0;
}

- (NSUInteger)totalCodes
{
    if (nextCode == nil)
    {
        return 1;
    }
    
    return nextCode.totalCodes + 1;
}

@end
