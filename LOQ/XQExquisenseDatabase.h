//
//  XQExquisenseDatabase.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/8/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DATABASE_KEY @"Database"

@interface XQExquisenseDatabase : NSObject <NSCoding>

#pragma mark - Initializers

- (id)init;

#pragma mark - Table

// Write a table with given tableID to the database, replacing an existing table with the same tableID
- (void)setTableForKey:(NSString *)tableID;

// Write a table to the database, replacing an existing table with the same tableID
- (void)setTable:(NSDictionary *)table forKey:(NSString *)tableID;

// Retrieve a table with specified tableID from the database
- (NSDictionary *)getTableForKey:(NSString *)tableID;

// Delete a table with specified tableID from the database
- (void)removeTableForKey:(NSString *)tableID;

// Retrieve all tables from the database
- (NSArray *)getTables;

#pragma mark - Entry

// Write an entry to specified table with corresponding tableID,
// replacing an existing entry with the same entryID
- (void)setEntry:(NSDictionary *)entry forKey:(NSString *)entryID toTable:(NSString *)tableID;

// Retrieve an entry with specified entryID from specified table with corresponding tableID
- (NSDictionary *)getEntryForKey:(NSString *)entryID fromTable:(NSString *)tableID;

// Delete an entry with specified entryID from specified table with corresponding tableID
- (void)removeEntryForKey:(NSString *)entryID fromTable:(NSString *)tableID;

// Retrieve all entries from specified table with corresponding tableID
- (NSArray *)getEntriesFromTable:(NSString *)tableID;

#pragma mark - Debugging Methods

- (void)printCurrentState;
 
@end
