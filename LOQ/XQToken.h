//
//  XQToken.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/10/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//
//  Adapted from FreeOTP by Nathaniel McCallum <npmccallum@redhat.com> with several modifications.
//  In compliance with Apache License v2.0, as required by licensor.
//  Copyright (C) 2013 Nathaniel McCallum, RedHat.
//

#import <Foundation/Foundation.h>
#import "XQTokenCode.h"

@interface XQToken : NSObject

@property (strong, nonatomic) XQTokenCode *tokenCode;

- (id)initWithTokenDetails:(NSDictionary *)tokenDetails;

@end
