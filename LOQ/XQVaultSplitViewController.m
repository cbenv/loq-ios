//
//  XQVaultSplitViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/12/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQVaultSplitViewController.h"

#define TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE @"toEditEntryTableViewControllerSegue"

@interface XQVaultSplitViewController () <XQVaultSplitViewControllerDelegate>
{
    XQVaultTableViewController *masterViewController;
    XQEntryDetailViewController *detailViewController;
}

- (IBAction)addBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)editBarButtonPressed:(UIBarButtonItem *)sender;

@end

@implementation XQVaultSplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    [backgroundView setBackgroundColor:[XQExquisense exquisenseOrange]];
    [self.view insertSubview:backgroundView atIndex:0];
    
    UINavigationController *navigationController = [self.viewControllers objectAtIndex:0];
    masterViewController = (XQVaultTableViewController *)[navigationController topViewController];
    detailViewController = [self.viewControllers objectAtIndex:1];
    
    [masterViewController setDelegate:detailViewController];
    [detailViewController setDelegate:masterViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)addBarButtonPressed:(UIBarButtonItem *)sender
{
    [self performSegueWithIdentifier:TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE sender:sender];
}

- (IBAction)editBarButtonPressed:(UIBarButtonItem *)sender
{
    [detailViewController editBarButtonPressed:sender];
}

@end
