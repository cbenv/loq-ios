//
//  XQExquisenseTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/4/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseTableViewController.h"

@interface XQExquisenseTableViewController ()

@end

@implementation XQExquisenseTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // [self.view setBackgroundColor:[XQExquisense exquisenseOrangeComplement]];
    [self.tableView setSeparatorColor:[XQExquisense exquisenseOrangeComplement]];
    [self.tableView setSectionIndexColor:[XQExquisense exquisenseOrange]];
    [self.tableView setTintColor:[XQExquisense exquisenseOrange]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

@end
