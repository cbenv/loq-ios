//
//  XQVaultTableViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/7/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseTableViewController.h"
#import "XQVaultSplitViewController.h"

@interface XQVaultTableViewController : XQExquisenseTableViewController

@property (weak, nonatomic) id delegate;

- (IBAction)addBarButtonPressed:(UIBarButtonItem *)sender;

@end
