//
//  XQPasswordGeneratorTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/4/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQPasswordGeneratorTableViewController.h"
#import "XQEditEntryTableViewController.h"
#import "XQEditEntryAndSecondAuthenticationTableViewController.h"
#import "XQExquisenseSecurity.h"
#import "XQPasswordGeneratorLengthViewController.h"

#define TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE @"toEditEntryTableViewControllerSegue"
#define EMBED_PASSWORD_GENERATOR_LENGTH_VIEW_CONTROLLER_SEGUE @"embedPasswordGeneratorLengthViewController"

@interface XQPasswordGeneratorTableViewController ()
{
    XQPasswordGeneratorLengthViewController *passwordGeneratorLengthViewController;
    
    int passwordLength;
    BOOL useLowercaseAlphabets;
    BOOL useUppercaseAlphabets;
    BOOL useNumbers;
    BOOL useSpecialCharacters;
    BOOL useAmbiguousCharacters;
}

@property (strong, nonatomic) IBOutlet UIView *passwordLengthView;
@property (strong, nonatomic) IBOutlet UITableViewCell *useLowercaseAlphabetsCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *useUppercaseAlphabetsCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *useNumbersCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *useSpecialCharactersCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *useAmbiguousCharactersCell;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;

- (IBAction)copyButtonPressed:(UIButton *)sender;
- (IBAction)generateButtonPressed:(UIButton *)sender;
- (IBAction)addButtonPressed:(UIButton *)sender;

@end

@implementation XQPasswordGeneratorTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    passwordGeneratorLengthViewController = [self.childViewControllers lastObject];
    
    [self loadPasswordGeneratorSettings];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.passwordLabel setText:[self generatePassword]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self savePasswordGeneratorSettings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    NSLog(@"PasswordGeneratorViewController: %f", self.view.frame.size.width);
    
    [self.passwordLengthView setFrame:CGRectMake(self.passwordLengthView.frame.origin.x,
                                                 self.passwordLengthView.frame.origin.y,
                                                 UIInterfaceOrientationIsLandscape(self.interfaceOrientation) ? 768 : 1024,
                                                 self.passwordLengthView.frame.size.height)];
    
    NSLog(@"PasswordGeneratorLengthView: %f", self.passwordLengthView.frame.size.width);
}

#pragma mark - IBActions

- (IBAction)copyButtonPressed:(UIButton *)sender
{
    [[UIPasteboard generalPasteboard] setString:self.passwordLabel.text];
}

- (IBAction)generateButtonPressed:(UIButton *)sender
{
    [self savePasswordGeneratorSettings];
    [self.passwordLabel setText:[self generatePassword]];
}

- (IBAction)addButtonPressed:(UIButton *)sender
{
    [self performSegueWithIdentifier:TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE sender:self];
}

#pragma mark - Segue Controllers

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE] &&
        [segue.destinationViewController isKindOfClass:[XQEditEntryTableViewController class]])
    {
        XQEditEntryTableViewController *editEntryTableViewController = segue.destinationViewController;
        NSDictionary *entryDetails = [NSDictionary dictionaryWithObject:self.passwordLabel.text
                                                                 forKey:ENTRY_PASSWORD];
        
        [editEntryTableViewController setEntryDetails:entryDetails];
    }
    if ([segue.identifier isEqualToString:TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE] &&
        [segue.destinationViewController isKindOfClass:[XQEditEntryAndSecondAuthenticationTableViewController class]])
    {
        XQEditEntryAndSecondAuthenticationTableViewController *editEntryAndSecondAuthenticationTableViewController = segue.destinationViewController;
        NSDictionary *entryDetails = [NSDictionary dictionaryWithObject:self.passwordLabel.text
                                                                 forKey:ENTRY_PASSWORD];
        
        [editEntryAndSecondAuthenticationTableViewController setEntryDetails:entryDetails];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0)
    {
        [cell setAccessoryType:cell.accessoryType == UITableViewCellAccessoryNone ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone];
        
        if (indexPath.row == 0)
        {
            useLowercaseAlphabets = !useLowercaseAlphabets;
        }
        else if (indexPath.row == 1)
        {
            useUppercaseAlphabets = !useUppercaseAlphabets;
        }
        else if (indexPath.row == 2)
        {
            useNumbers = !useNumbers;
        }
        else if (indexPath.row == 3)
        {
            useSpecialCharacters = !useSpecialCharacters;
        }
        else if (indexPath.row == 4)
        {
            useAmbiguousCharacters = !useAmbiguousCharacters;
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            [self.passwordLabel setText:[self generatePassword]];
        }
    }
}

#pragma mark - Helper Methods

- (NSString *)generatePassword
{
    [self savePasswordGeneratorSettings];
    
    return [XQExquisenseSecurity generatePassword];
}

- (void)savePasswordGeneratorSettings
{
    NSMutableDictionary *passwordGeneratorSettings = [[NSMutableDictionary alloc] init];
    
    [passwordGeneratorSettings setValue:@([passwordGeneratorLengthViewController getPasswordLength])
                                 forKey:PASSWORD_GENERATOR_PASSWORD_LENGTH];
    [passwordGeneratorSettings setValue:@(useLowercaseAlphabets)
                                 forKey:PASSWORD_GENERATOR_USE_LOWERCASE_ALPHABETS];
    [passwordGeneratorSettings setValue:@(useUppercaseAlphabets)
                                 forKey:PASSWORD_GENERATOR_USE_UPPERCASE_ALPHABETS];
    [passwordGeneratorSettings setValue:@(useNumbers)
                                 forKey:PASSWORD_GENERATOR_USE_NUMBERS];
    [passwordGeneratorSettings setValue:@(useSpecialCharacters)
                                 forKey:PASSWORD_GENERATOR_USE_SPECIAL_CHARACTERS];
    [passwordGeneratorSettings setValue:@(useAmbiguousCharacters)
                                 forKey:PASSWORD_GENERATOR_USE_AMBIGUOUS_CHARACTERS];
    
    [[NSUserDefaults standardUserDefaults] setObject:passwordGeneratorSettings
                                              forKey:PASSWORD_GENERATOR_SETTINGS_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadPasswordGeneratorSettings
{
    NSDictionary *passwordGeneratorSettings = [[NSUserDefaults standardUserDefaults]
                                               dictionaryForKey:PASSWORD_GENERATOR_SETTINGS_KEY];
    
    if (passwordGeneratorSettings)
    {
        passwordLength = [[passwordGeneratorSettings valueForKey:PASSWORD_GENERATOR_PASSWORD_LENGTH] intValue];
        
        useLowercaseAlphabets = [[passwordGeneratorSettings valueForKey:
                                  PASSWORD_GENERATOR_USE_LOWERCASE_ALPHABETS] boolValue];
        useUppercaseAlphabets = [[passwordGeneratorSettings valueForKey:
                                  PASSWORD_GENERATOR_USE_UPPERCASE_ALPHABETS] boolValue];
        useNumbers = [[passwordGeneratorSettings valueForKey:
                       PASSWORD_GENERATOR_USE_NUMBERS] boolValue];
        useSpecialCharacters = [[passwordGeneratorSettings valueForKey:
                                 PASSWORD_GENERATOR_USE_SPECIAL_CHARACTERS] boolValue];
        useAmbiguousCharacters = [[passwordGeneratorSettings valueForKey:
                                   PASSWORD_GENERATOR_USE_AMBIGUOUS_CHARACTERS] boolValue];
    }
    else
    {
        passwordLength = ENTRY_PASSWORD_LENGTH_DEFAULT;
        
        useLowercaseAlphabets = YES;
        useUppercaseAlphabets = YES;
        useNumbers = YES;
        useSpecialCharacters = YES;
        useAmbiguousCharacters = NO;
    }
    
    int selected = UITableViewCellAccessoryCheckmark;
    int unselected = UITableViewCellAccessoryNone;
    
    [self.useLowercaseAlphabetsCell setAccessoryType:useLowercaseAlphabets ? selected : unselected];
    [self.useUppercaseAlphabetsCell setAccessoryType:useUppercaseAlphabets ? selected : unselected];
    [self.useNumbersCell setAccessoryType:useNumbers ? selected : unselected];
    [self.useSpecialCharactersCell setAccessoryType:useSpecialCharacters ? selected : unselected];
    [self.useAmbiguousCharactersCell setAccessoryType:useAmbiguousCharacters ? selected : unselected];
}

@end
