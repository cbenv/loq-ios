//
//  XQEditEntryAndSecondAuthenticationTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/13/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQEditEntryAndSecondAuthenticationTableViewController.h"
#import "XQScanQRCodeViewController.h"
#import "XQExquisenseSecurity.h"
#import "XQAppDelegate.h"

#define TO_QR_SCANNER_VIEW_CONTROLLER_SEGUE @"toQRScannerViewControllerSegue"

@interface XQEditEntryAndSecondAuthenticationTableViewController () <UITextFieldDelegate, XQScanQRCodeViewControllerDelegate>
{
    XQAppDelegate *appDelegate;
    
    NSMutableDictionary *entryDetailsTemp;
    NSMutableDictionary *entryTokenTemp;
    
    BOOL enableTwoStepsAuthentication;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveBarButton;
@property (strong, nonatomic) IBOutlet UIButton *scanQRCodeButton;

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *idTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) IBOutlet UITableViewCell *enableTwoStepsAuthenticationCell;

@property (strong, nonatomic) IBOutlet UITableViewCell *tokenTypeTOTPCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *tokenTypeHOTPCell;

@property (strong, nonatomic) IBOutlet UITextField *tokenIssuerTextField;
@property (strong, nonatomic) IBOutlet UITextField *tokenIDTextField;
@property (strong, nonatomic) IBOutlet UITextField *tokenSecretTextField;
@property (strong, nonatomic) IBOutlet UISegmentedControl *tokenSecretType;
@property (strong, nonatomic) IBOutlet UILabel *tokenIntervalCounterLabel;
@property (strong, nonatomic) IBOutlet UILabel *tokenDigitsLabel;
@property (strong, nonatomic) IBOutlet UILabel *intervalCounterLabel;
@property (strong, nonatomic) IBOutlet UIStepper *intervalCounterStepper;
@property (strong, nonatomic) IBOutlet UIStepper *digitsStepper;

@property (strong, nonatomic) IBOutlet UITableViewCell *tokenAlgorithmMD5Cell;
@property (strong, nonatomic) IBOutlet UITableViewCell *tokenAlgorithmSHA1Cell;
@property (strong, nonatomic) IBOutlet UITableViewCell *tokenAlgorithmSHA256Cell;
@property (strong, nonatomic) IBOutlet UITableViewCell *tokenAlgorithmSHA512Cell;

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)saveBarButtonPressed:(UIBarButtonItem *)sender;

- (IBAction)generatePasswordButtonPressed:(UIButton *)sender;
- (IBAction)pasteButtonPressed:(UIButton *)sender;

- (IBAction)scanQRCodeButtonPressed:(UIButton *)sender;

- (IBAction)tokenSecretTypeChanged:(UISegmentedControl *)sender;
- (IBAction)intervalCounterStepperChanged:(UIStepper *)sender;
- (IBAction)digitStepperChanged:(UIStepper *)sender;

- (IBAction)requiredFieldFilled:(id)sender;

@end

@implementation XQEditEntryAndSecondAuthenticationTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    [self.nameTextField setDelegate:self];
    [self.idTextField setDelegate:self];
    [self.passwordTextField setDelegate:self];
    [self.tokenIssuerTextField setDelegate:self];
    [self.tokenIDTextField setDelegate:self];
    [self.tokenSecretTextField setDelegate:self];
    [self.passwordTextField setSecureTextEntry:YES];
    [self.nameTextField setReturnKeyType:UIReturnKeyNext];
    [self.idTextField setReturnKeyType:UIReturnKeyNext];
    [self.passwordTextField setReturnKeyType:UIReturnKeyDone];
    [self.tokenIssuerTextField setReturnKeyType:UIReturnKeyNext];
    [self.tokenIDTextField setReturnKeyType:UIReturnKeyNext];
    [self.tokenSecretTextField setReturnKeyType:UIReturnKeyDone];
    [self.tokenSecretType setSelectedSegmentIndex:1];
    [self.tokenSecretTextField setPlaceholder:@"32 base-32 characters"];
    [self.digitsStepper setMinimumValue:ENTRY_TOKEN_DIGITS_MINIMUM];
    [self.digitsStepper setMaximumValue:ENTRY_TOKEN_DIGITS_MAXIMUM];
    
    enableTwoStepsAuthentication = NO;
    
    if (self.entryDetails)
    {
        entryDetailsTemp = [self.entryDetails mutableCopy];
        
        [self.nameTextField setText:[entryDetailsTemp valueForKey:ENTRY_NAME]];
        [self.idTextField setText:[entryDetailsTemp valueForKey:ENTRY_ID]];
        [self.passwordTextField setText:[entryDetailsTemp valueForKey:ENTRY_PASSWORD]];
        
        enableTwoStepsAuthentication = (BOOL) [entryDetailsTemp valueForKey:ENTRY_TOKEN];
        
        [self.enableTwoStepsAuthenticationCell setAccessoryType: enableTwoStepsAuthentication ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone];
        [self.scanQRCodeButton setHidden:!enableTwoStepsAuthentication];
        
        if (enableTwoStepsAuthentication)
        {
            entryTokenTemp = [[entryDetailsTemp valueForKey:ENTRY_TOKEN] mutableCopy];
            
            NSString *tokenType = [entryTokenTemp valueForKey:ENTRY_TOKEN_TYPE];
            NSString *tokenAlgorithm = [entryTokenTemp valueForKey:ENTRY_TOKEN_ALGORITHM];
            NSString *tokenSecretType = [entryTokenTemp valueForKey:ENTRY_TOKEN_SECRET_TYPE];
            
            if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_TOTP])
            {
                [self.tokenTypeTOTPCell setAccessoryType:UITableViewCellAccessoryCheckmark];
                [self.intervalCounterLabel setText:@"Interval"];
                
                [self.intervalCounterStepper setValue:[[entryTokenTemp valueForKey:ENTRY_TOKEN_INTERVAL] intValue]];
                [self.intervalCounterStepper setMaximumValue:ENTRY_TOKEN_INTERVAL_MAXIMUM];
                [self.intervalCounterStepper setMinimumValue:ENTRY_TOKEN_INTERVAL_MINIMUM];
            }
            else if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_HOTP])
            {
                [self.tokenTypeHOTPCell setAccessoryType:UITableViewCellAccessoryCheckmark];
                [self.intervalCounterLabel setText:@"Counter"];
                
                [self.intervalCounterStepper setValue:[[entryTokenTemp valueForKey:ENTRY_TOKEN_COUNTER] intValue]];
                [self.intervalCounterStepper setMaximumValue:ENTRY_TOKEN_COUNTER_MAXIMUM];
                [self.intervalCounterStepper setMinimumValue:ENTRY_TOKEN_COUNTER_MINIMUM];
            }
            
            if ([tokenAlgorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_MD5])
            {
                [self.tokenAlgorithmMD5Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else if ([tokenAlgorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA1])
            {
                [self.tokenAlgorithmSHA1Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else if ([tokenAlgorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA256])
            {
                [self.tokenAlgorithmSHA256Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            else if ([tokenAlgorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA512])
            {
                [self.tokenAlgorithmSHA512Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            
            if ([tokenSecretType isEqualToString:ENTRY_TOKEN_SECRET_TYPE_HEX])
            {
                [self.tokenSecretType setSelectedSegmentIndex:0];
                [self.tokenSecretTextField setPlaceholder:@"40 hexadecimal characters"];
            }
            else if ([tokenSecretType isEqualToString:ENTRY_TOKEN_SECRET_TYPE_BASE32])
            {
                [self.tokenSecretType setSelectedSegmentIndex:1];
                [self.tokenSecretTextField setPlaceholder:@"32 base-32 characters"];
            }
            
            [self.tokenIssuerTextField setText:[entryTokenTemp valueForKey:ENTRY_TOKEN_ISSUER]];
            [self.tokenIDTextField setText:[entryTokenTemp valueForKey:ENTRY_TOKEN_ID]];
            [self.tokenSecretTextField setText:[entryTokenTemp valueForKey:ENTRY_TOKEN_SECRET]];
            [self.digitsStepper setValue:[[entryTokenTemp valueForKey:ENTRY_TOKEN_DIGITS] intValue]];
            
            [self intervalCounterStepperChanged:self.intervalCounterStepper];
            [self digitStepperChanged:self.digitsStepper];
        }
    }
    else
    {
        entryDetailsTemp = [[NSMutableDictionary alloc] init];
        entryTokenTemp = [[NSMutableDictionary alloc] init];
        
        [self.intervalCounterLabel setText:@"Interval"];
        [self.intervalCounterStepper setValue:ENTRY_TOKEN_INTERVAL_DEFAULT];
        [self.digitsStepper setValue:ENTRY_TOKEN_DIGITS_DEFAULT];
        [self intervalCounterStepperChanged:self.intervalCounterStepper];
        [self digitStepperChanged:self.digitsStepper];
    }
    
    [self requiredFieldFilled:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveBarButtonPressed:(UIBarButtonItem *)sender
{
    if (enableTwoStepsAuthentication)
    {
        [entryTokenTemp setValue:self.tokenIssuerTextField.text forKey:ENTRY_TOKEN_ISSUER];
        [entryTokenTemp setValue:self.tokenIDTextField.text forKey:ENTRY_TOKEN_ID];
        [entryTokenTemp setValue:self.tokenSecretTextField.text forKey:ENTRY_TOKEN_SECRET];
        [entryTokenTemp setValue:@(self.digitsStepper.value) forKey:ENTRY_TOKEN_DIGITS];
        
        NSString *tokenType = [entryTokenTemp valueForKey:ENTRY_TOKEN_TYPE];
        if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_TOTP])
        {
            [entryTokenTemp setValue:@(self.intervalCounterStepper.value) forKey:ENTRY_TOKEN_INTERVAL];
            [entryTokenTemp setValue:nil forKey:ENTRY_TOKEN_COUNTER];
        }
        else if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_HOTP])
        {
            [entryTokenTemp setValue:@(self.intervalCounterStepper.value) forKey:ENTRY_TOKEN_COUNTER];
            [entryTokenTemp setValue:nil forKey:ENTRY_TOKEN_INTERVAL];
        }
        
        [entryDetailsTemp setObject:entryTokenTemp forKey:ENTRY_TOKEN];
    }
    else
    {
        // entryTokenTemp = nil;
        
        [entryDetailsTemp removeObjectForKey:ENTRY_TOKEN];
    }
    
    NSLog(@"Here 1");
    
    [entryDetailsTemp setObject:self.nameTextField.text forKey:ENTRY_NAME];
    [entryDetailsTemp setObject:self.idTextField.text forKey:ENTRY_ID];
    [entryDetailsTemp setObject:self.passwordTextField.text forKey:ENTRY_PASSWORD];
    // [entryDetailsTemp setObject:entryTokenTemp forKey:ENTRY_TOKEN];
    
    NSLog(@"Here 2");
    
    XQExquisenseDatabase *database = [appDelegate database];
    
    if ([self.entryDetails objectForKey:ENTRY_NAME])
    {
        [database removeEntryForKey:[self.entryDetails objectForKey:ENTRY_NAME] fromTable:TABLE_NAME];
    }
    
    [database setEntry:entryDetailsTemp forKey:[entryDetailsTemp objectForKey:ENTRY_NAME] toTable:TABLE_NAME];
    
    NSLog(@"Here 3");
    
    if (ENABLE_LOGGING) [database printCurrentState];
    
    NSString *hashedPassword = [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
    [appDelegate saveDatabaseWithPassword:hashedPassword];
    
    [self.delegate didEditEntry:entryDetailsTemp];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)generatePasswordButtonPressed:(UIButton *)sender
{
    [self.passwordTextField setText:[XQExquisenseSecurity generatePassword]];
}

- (IBAction)pasteButtonPressed:(UIButton *)sender
{
    [self.passwordTextField setText:[UIPasteboard generalPasteboard].string];
}

- (IBAction)scanQRCodeButtonPressed:(UIButton *)sender
{
    [self performSegueWithIdentifier:TO_QR_SCANNER_VIEW_CONTROLLER_SEGUE sender:self];
}

- (IBAction)tokenSecretTypeChanged:(UISegmentedControl *)sender
{
    [self.tokenSecretTextField setText:@""];
    
    if ([self.tokenSecretType selectedSegmentIndex] == 0)
    {
        [self.tokenSecretTextField setPlaceholder:@"40 hexadecimal characters"];
    }
    else if ([self.tokenSecretType selectedSegmentIndex] == 1)
    {
        [self.tokenSecretTextField setPlaceholder:@"32 base-32 characters"];
    }
}

- (IBAction)intervalCounterStepperChanged:(UIStepper *)sender
{
    [self.tokenIntervalCounterLabel setText:[NSString stringWithFormat:@"%i", (int) sender.value]];
}

- (IBAction)digitStepperChanged:(UIStepper *)sender
{
    [self.tokenDigitsLabel setText:[NSString stringWithFormat:@"%i", (int) sender.value]];
}

- (IBAction)requiredFieldFilled:(id)sender
{
    // if (!([self.nameTextField.text isEqualToString:@""] ||
    //      [self.idTextField.text isEqualToString:@""] ||
    //      [self.passwordTextField.text isEqualToString:@""]))
    if (![self.nameTextField.text isEqualToString:@""])
    {
        [self.saveBarButton setEnabled:YES];
    }
    else
    {
        [self.saveBarButton setEnabled:NO];
    }
    
    if (enableTwoStepsAuthentication)
    {
        if ([self.tokenSecretType selectedSegmentIndex] == 0 && [self.tokenSecretTextField.text length] == 40)
        {
            [self.saveBarButton setEnabled:YES];
        }
        else if ([self.tokenSecretType selectedSegmentIndex] == 1 && [self.tokenSecretTextField.text length] == 32)
        {
            [self.saveBarButton setEnabled:YES];
        }
        else
        {
            [self.saveBarButton setEnabled:NO];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.nameTextField])
    {
        [self.idTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.idTextField])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.passwordTextField])
    {
        [self.passwordTextField resignFirstResponder];
    }
    else if ([textField isEqual:self.tokenIssuerTextField])
    {
        [self.tokenIDTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.tokenIDTextField])
    {
        [self.tokenSecretTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.tokenSecretTextField])
    {
        [self.tokenSecretTextField resignFirstResponder];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:self.tokenSecretTextField])
    {
        NSString* newstr = [textField.text stringByReplacingCharactersInRange:range withString:string];
        const char *newbuf = [newstr UTF8String];
        
        BOOL unpadded = NO;
        for (unsigned long i = [newstr length]; i > 0; i--)
        {
            if (!unpadded)
            {
                if (newbuf[i - 1] == '=')
                {
                    continue;
                }
                else
                {
                    unpadded = YES;
                }
            }
            
            if ([self.tokenSecretType selectedSegmentIndex] == 0)
            {
                if (!strchr("abdefABCDEF0123456789", newbuf[i - 1]))
                {
                    return NO;
                }
            }
            else // if ([self.tokenSecretType selectedSegmentIndex] == 1)
            {
                if (!strchr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ234567", newbuf[i - 1]))
                {
                    return NO;
                }
            }
        }
    }
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return enableTwoStepsAuthentication ? 5 : 2;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            [self.nameTextField becomeFirstResponder];
        }
        else if (indexPath.row == 1)
        {
            [self.idTextField becomeFirstResponder];
        }
        else if (indexPath.row == 2)
        {
            [self.passwordTextField becomeFirstResponder];
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            enableTwoStepsAuthentication = !enableTwoStepsAuthentication;
            [self.enableTwoStepsAuthenticationCell setAccessoryType: enableTwoStepsAuthentication ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone];
            [self.scanQRCodeButton setHidden:!enableTwoStepsAuthentication];
            
            [self.tableView reloadData];
            [self requiredFieldFilled:self];
        }
    }
    else if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
            [self.tokenTypeHOTPCell setAccessoryType:UITableViewCellAccessoryNone];
            [self.tokenTypeTOTPCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [self.intervalCounterLabel setText:@"Interval"];
            [self.intervalCounterStepper setMaximumValue:ENTRY_TOKEN_INTERVAL_MAXIMUM];
            [self.intervalCounterStepper setMinimumValue:ENTRY_TOKEN_INTERVAL_MINIMUM];
            [self.intervalCounterStepper setValue:ENTRY_TOKEN_INTERVAL_DEFAULT];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_TYPE_TOTP forKey:ENTRY_TOKEN_TYPE];
        }
        else if (indexPath.row == 1)
        {
            [self.tokenTypeTOTPCell setAccessoryType:UITableViewCellAccessoryNone];
            [self.tokenTypeHOTPCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [self.intervalCounterLabel setText:@"Counter"];
            [self.intervalCounterStepper setMaximumValue:ENTRY_TOKEN_COUNTER_MAXIMUM];
            [self.intervalCounterStepper setMinimumValue:ENTRY_TOKEN_COUNTER_MINIMUM];
            [self.intervalCounterStepper setValue:ENTRY_TOKEN_COUNTER_DEFAULT];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_TYPE_HOTP forKey:ENTRY_TOKEN_TYPE];
        }
        
        [self intervalCounterStepperChanged:self.intervalCounterStepper];
    }
    else if (indexPath.section == 3)
    {
        if (indexPath.row == 0)
        {
            [self.tokenIssuerTextField becomeFirstResponder];
        }
        else if (indexPath.row == 1)
        {
            [self.tokenIDTextField becomeFirstResponder];
        }
        else if (indexPath.row == 2)
        {
            [self.tokenSecretTextField becomeFirstResponder];
        }
    }
    else if (indexPath.section == 4)
    {
        [self.tokenAlgorithmMD5Cell setAccessoryType:UITableViewCellAccessoryNone];
        [self.tokenAlgorithmSHA1Cell setAccessoryType:UITableViewCellAccessoryNone];
        [self.tokenAlgorithmSHA256Cell setAccessoryType:UITableViewCellAccessoryNone];
        [self.tokenAlgorithmSHA512Cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if (indexPath.row == 0)
        {
            [self.tokenAlgorithmMD5Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_ALGORITHM_MD5 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if (indexPath.row == 1)
        {
            [self.tokenAlgorithmSHA1Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_ALGORITHM_SHA1 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if (indexPath.row == 2)
        {
            [self.tokenAlgorithmSHA256Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_ALGORITHM_SHA256 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if (indexPath.row == 3)
        {
            [self.tokenAlgorithmSHA512Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_ALGORITHM_SHA512 forKey:ENTRY_TOKEN_ALGORITHM];
        }
    }
}

#pragma mark - XQScanQRCodeViewControllerDelegate

- (void)didConfigure:(NSDictionary *)entryToken
{
    if (entryToken)
    {
        [entryDetailsTemp setValue:entryToken forKey:ENTRY_TOKEN];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Configuration Error"
                                                        message:@"Invalid QR Code."
                                                       delegate:nil
                                              cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

@end
