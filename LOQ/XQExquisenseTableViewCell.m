//
//  XQExquisenseTableViewCell.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/4/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseTableViewCell.h"

@implementation XQExquisenseTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
