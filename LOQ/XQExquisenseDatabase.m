//
//  XQExquisenseDatabase.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/8/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseDatabase.h"
#import "XQExquisenseSecurity.h"

#define DATABASE_KEY @"Database"
#define TABLES_KEY @"Tables"

@implementation XQExquisenseDatabase
{
    NSMutableDictionary *tables;
}

#pragma mark - Initializers

- (id)init
{
    self = [super init];
    if (self)
    {
        tables = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark - Table

// Write a table with given tableID to the database, replacing an existing table with the same tableID
- (void)setTableForKey:(NSString *)tableID
{
    [self setTable:[NSMutableDictionary dictionary] forKey:tableID];
}

// Write a table to the database, replacing an existing table with the same tableID
- (void)setTable:(NSDictionary *)table forKey:(NSString *)tableID
{
    [tables setObject:[NSMutableDictionary dictionaryWithDictionary:tables] forKey:tableID];
}

// Retrieve a table with specified tableID from the database
- (NSDictionary *)getTableForKey:(NSString *)tableID
{
    return [NSDictionary dictionaryWithDictionary:[tables objectForKey:tableID]];
}

// Delete a table with specified tableID from the database
- (void)removeTableForKey:(NSString *)tableID
{
    [tables removeObjectForKey:tableID];
}

// Retrieve all tables from the database
- (NSArray *)getTables
{
    return [tables allValues];
}

#pragma mark - Entry

// Write an entry to specified table with corresponding tableID,
// replacing an existing entry with the same entryID
- (void)setEntry:(NSDictionary *)entry forKey:(NSString *)entryID toTable:(NSString *)tableID
{
    [[tables objectForKey:tableID] setObject:[NSMutableDictionary dictionaryWithDictionary:entry] forKey:entryID];
}

// Retrieve an entry with specified entryID from specified table with corresponding tableID
- (NSDictionary *)getEntryForKey:(NSString *)entryID fromTable:(NSString *)tableID
{
    return [NSDictionary dictionaryWithDictionary:[[tables objectForKey:tableID] objectForKey:entryID]];
}

// Delete an entry with specified entryID from specified table with corresponding tableID
- (void)removeEntryForKey:(NSString *)entryID fromTable:(NSString *)tableID
{
    [[tables objectForKey:tableID] removeObjectForKey:entryID];
}

// Retrieve all entries from specified table with corresponding tableID
- (NSArray *)getEntriesFromTable:(NSString *)tableID
{
    return [[tables objectForKey:tableID] allValues];
}

#pragma mark - NSCoding Protocol

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:tables forKey:TABLES_KEY];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self == [super init])
    {
        tables = [aDecoder decodeObjectForKey:TABLES_KEY];
    }
    return self;
}

#pragma mark - Debugging Methods

- (void)printCurrentState
{
    NSLog(@"Current database state: \n%@", tables);
}

@end
