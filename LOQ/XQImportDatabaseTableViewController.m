//
//  XQImportDatabaseTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/17/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQImportDatabaseTableViewController.h"
#import "XQAppDelegate.h"

@interface XQImportDatabaseTableViewController () <UIAlertViewDelegate>
{
    NSArray *backups;
}

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)clearBarButtonPressed:(UIBarButtonItem *)sender;

@end

@implementation XQImportDatabaseTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *documentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    
    NSError *error;
    backups = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clearBarButtonPressed:(UIBarButtonItem *)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Deletion"
                                                    message:@"Are you sure?"
                                                   delegate:self
                                          cancelButtonTitle:@"Yes"
                                          otherButtonTitles:@"No", nil];
    [alert show];
}

#pragma UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        NSString *documentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        
        NSError *error;
        
        for (NSString *fileName in backups)
        {
            NSString *path = [documentsPath stringByAppendingPathComponent:fileName];
            [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        }
        
        backups = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
        [self.tableView reloadData];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [backups count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"importCell"];
    
    NSString *fileName = [backups objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:fileName];
    [cell.textLabel setTextColor:[XQExquisense exquisenseOrange]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *documentsPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *fileName = [backups objectAtIndex:indexPath.row];
    NSString *path = [documentsPath stringByAppendingPathComponent:fileName];
    
    NSData *data = [NSData dataWithContentsOfFile:path];
    
    if (ENABLE_LOGGING) NSLog(@"%@ importing data from %@", data ? @"Success" : @"Failed", path);
    
    if (data)
    {
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:DATABASE_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *hashedPassword = [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
        [(XQAppDelegate *)[[UIApplication sharedApplication] delegate] loadDatabaseWithPassword:hashedPassword];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Import Successful"
                                                        message:@"Database has been successfully imported."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

@end
