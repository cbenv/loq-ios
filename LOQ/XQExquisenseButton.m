//
//  XQExquisenseButton.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/4/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseButton.h"

@implementation XQExquisenseButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

- (void)setTitleFont:(UIFont *)font
{
    if (_titleFont != font)
    {
        _titleFont = font;
        self.titleLabel.font = font;
    }
}

@end
