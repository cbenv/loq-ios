//
//  XQScanQRCodeViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/6/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//
//  Adapted from FreeOTP by Nathaniel McCallum <npmccallum@redhat.com> with several modifications.
//  In compliance with Apache License v2.0, as required by licensor.
//  Copyright (C) 2013 Nathaniel McCallum, RedHat.
//

#import "XQScanQRCodeViewController.h"
@import AVFoundation;

#define ENTRY_TOKEN_AUTHENTICATION_OTPAUTH @"otpauth"

@interface XQScanQRCodeViewController () <AVCaptureMetadataOutputObjectsDelegate>
{
    NSMutableDictionary *entryTokenTemp;
}

@property (strong) AVCaptureSession *session;

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender;

@end

@implementation XQScanQRCodeViewController

#pragma mark - Static Methods

static BOOL ishex(char c)
{
    if (c >= '0' && c <= '9')
    {
        return YES;
    }
    if (c >= 'A' && c <= 'F')
    {
        return YES;
    }
    if (c >= 'a' && c <= 'f')
    {
        return YES;
    }
    
    return NO;
}

static uint8_t fromhex(char c)
{
    if (c >= '0' && c <= '9')
    {
        return c - '0';
    }
    if (c >= 'A' && c <= 'F')
    {
        return c - 'A' + 10;
    }
    if (c >= 'a' && c <= 'f')
    {
        return c - 'a' + 10;
    }
    
    return 0;
}

static NSString* decode(const NSString* str)
{
    if (str == nil)
    {
        return nil;
    }
    
    const char *tmp = [str UTF8String];
    NSMutableString *ret = [[NSMutableString alloc] init];
    for (NSUInteger i = 0; i < str.length; i++)
    {
        if (tmp[i] != '%' || !ishex(tmp[i + 1]) || !ishex(tmp[i + 2]))
        {
            [ret appendFormat:@"%c", tmp[i]];
            continue;
        }
        
        uint8_t c = 0;
        c |= fromhex(tmp[++i]) << 4;
        c |= fromhex(tmp[++i]);
        
        [ret appendFormat:@"%c", c];
    }
    
    return ret;
}

# pragma mark -

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.session = [[AVCaptureSession alloc] init];
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError* error = nil;
    AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (input == nil)
    {
        [self.navigationController popViewControllerAnimated:TRUE];
        return;
    }
    [self.session addInput:input];
    
    AVCaptureVideoPreviewLayer* preview = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    preview.frame = self.view.layer.bounds;
    [self.view.layer addSublayer:preview];
    
    [self.session startRunning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    AVCaptureMetadataOutput* output = [[AVCaptureMetadataOutput alloc] init];
    [self.session addOutput:output];
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [output setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - AVCaptureMetadataOutputObjectsDelegate

- (void)captureOutput:(AVCaptureOutput*)captureOutput didOutputMetadataObjects:(NSArray*)metadataObjects fromConnection:(AVCaptureConnection*) connection
{
    for (AVMetadataObject *metadata in metadataObjects)
    {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeQRCode])
        {
            NSString* qrcode = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            if (qrcode == nil)
            {
                continue;
            }
            
            if ([self parseQRCode:qrcode])
            {
                [self.delegate didConfigure:entryTokenTemp];
            }
            else
            {
                [self.delegate didConfigure:nil];
            }
            
            [self.session stopRunning];
            
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
    }
}

#pragma mark - Helper Methods

- (BOOL)parseQRCode:(NSString *)qrCode
{
    if (ENABLE_LOGGING) NSLog(@"Attempting to parse %@", qrCode);
    
    entryTokenTemp = [NSMutableDictionary dictionary];
    NSURL *url = [NSURL URLWithString:qrCode];
    
    NSString *scheme = [url scheme];
    if (scheme == nil || ![scheme isEqualToString:ENTRY_TOKEN_AUTHENTICATION_OTPAUTH])
    {
        return NO;
    }
    
    NSString *host = [url host];
    if (host == nil || (![host isEqualToString:@"totp"] &&
                        ![host isEqualToString:@"hotp"]))
    {
        return NO;
    }
    else
    {
        if ([host isEqualToString:@"totp"])
        {
            [entryTokenTemp setObject:ENTRY_TOKEN_TYPE_TOTP forKey:ENTRY_TOKEN_TYPE];
        }
        else
        {
            [entryTokenTemp setObject:ENTRY_TOKEN_TYPE_HOTP forKey:ENTRY_TOKEN_TYPE];
        }
    }
    
    NSString *path = [url path];
    if (path == nil)
    {
        return NO;
    }
    else
    {
        while ([path hasPrefix:@"/"])
        {
            path = [path substringFromIndex:1];
        }
        if ([path length] == 0)
        {
            return NO;
        }
    }
    
    NSArray *array = [path componentsSeparatedByString:@":"];
    
    if (array == nil || [array count] == 0)
    {
        return NO;
    }
    else if ([array count] > 1)
    {
        [entryTokenTemp setObject:decode([array objectAtIndex:0]) forKey:ENTRY_TOKEN_ISSUER];
        [entryTokenTemp setObject:decode([array objectAtIndex:1]) forKey:ENTRY_TOKEN_ID];
    }
    else
    {
        [entryTokenTemp setObject:decode([array objectAtIndex:0]) forKey:ENTRY_TOKEN_ID];
    }
    
    NSMutableDictionary *query = [NSMutableDictionary dictionary];
    array = [[url query] componentsSeparatedByString:@"&"];
    for (NSString *kv in array)
    {
        NSArray *kvPair = [kv componentsSeparatedByString:@"="];
        if ([kvPair count] == 2)
        {
            [query setObject:decode([kvPair objectAtIndex:1]) forKey:[kvPair objectAtIndex:0]];
        }
    }
    
    NSString *secret = [query objectForKey:@"secret"];
    
    if (secret == nil)
    {
        return NO;
    }
    else
    {
        [entryTokenTemp setObject:secret forKey:ENTRY_TOKEN_SECRET];
        [entryTokenTemp setObject:ENTRY_TOKEN_SECRET_TYPE_BASE32 forKey:ENTRY_TOKEN_SECRET_TYPE];
    }
    
    NSString *digits = [query objectForKey:@"digits"];
    
    if (digits == nil)
    {
        [entryTokenTemp setObject:@(ENTRY_TOKEN_DIGITS_DEFAULT) forKey:ENTRY_TOKEN_DIGITS];
    }
    else
    {
        NSInteger digitsValue = [digits integerValue];
        
        if (digitsValue < ENTRY_TOKEN_DIGITS_MINIMUM || digitsValue > ENTRY_TOKEN_DIGITS_MAXIMUM)
        {
            [entryTokenTemp setObject:@(ENTRY_TOKEN_DIGITS_DEFAULT) forKey:ENTRY_TOKEN_DIGITS];
        }
        else
        {
            [entryTokenTemp setObject:@(digitsValue) forKey:ENTRY_TOKEN_DIGITS];
        }
    }
    
    NSString *algorithm = [query objectForKey:@"algorithm"];
    
    if (algorithm == nil)
    {
        [entryTokenTemp setObject:ENTRY_TOKEN_ALGORITHM_SHA1 forKey:ENTRY_TOKEN_ALGORITHM];
    }
    else
    {
        if ([algorithm isEqualToString:@"md5"])
        {
            [entryTokenTemp setObject:ENTRY_TOKEN_ALGORITHM_MD5 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if ([algorithm isEqualToString:@"sha1"])
        {
            [entryTokenTemp setObject:ENTRY_TOKEN_ALGORITHM_SHA1 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if ([algorithm isEqualToString:@"sha256"])
        {
            [entryTokenTemp setObject:ENTRY_TOKEN_ALGORITHM_SHA256 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if ([algorithm isEqualToString:@"sha512"])
        {
            [entryTokenTemp setObject:ENTRY_TOKEN_ALGORITHM_SHA512 forKey:ENTRY_TOKEN_ALGORITHM];
        }
    }
        
    NSString *type = [entryTokenTemp objectForKey:ENTRY_TOKEN_TYPE];
    
    if ([type isEqualToString:ENTRY_TOKEN_TYPE_TOTP])
    {
        NSString *interval = [query objectForKey:@"interval"];
        
        if (interval == nil)
        {
            [entryTokenTemp setObject:@(ENTRY_TOKEN_INTERVAL_DEFAULT) forKey:ENTRY_TOKEN_INTERVAL];
        }
        else
        {
            NSUInteger intervalValue = [interval integerValue];
            
            if (intervalValue < ENTRY_TOKEN_INTERVAL_MINIMUM || intervalValue > ENTRY_TOKEN_INTERVAL_MAXIMUM)
            {
                [entryTokenTemp setObject:@(ENTRY_TOKEN_INTERVAL_DEFAULT) forKey:ENTRY_TOKEN_INTERVAL];
            }
            else
            {
                [entryTokenTemp setObject:@(intervalValue) forKey:ENTRY_TOKEN_INTERVAL];
            }
        }
    }
    else if ([type isEqualToString:ENTRY_TOKEN_TYPE_HOTP])
    {
        NSString *counter = [query objectForKey:@"counter"];
        
        if (counter == nil)
        {
            [entryTokenTemp setObject:@(ENTRY_TOKEN_COUNTER_DEFAULT) forKey:ENTRY_TOKEN_COUNTER];
        }
        else
        {
            NSUInteger counterValue = [counter integerValue];
            
            if (counterValue < ENTRY_TOKEN_COUNTER_MINIMUM || counterValue > ENTRY_TOKEN_COUNTER_MAXIMUM)
            {
                [entryTokenTemp setObject:@(ENTRY_TOKEN_COUNTER_DEFAULT) forKey:ENTRY_TOKEN_COUNTER];
            }
            else
            {
                [entryTokenTemp setObject:@(counterValue) forKey:ENTRY_TOKEN_COUNTER];
            }
        }
    }
    
    if (ENABLE_LOGGING) NSLog(@"Successful parsing yields \n%@", entryTokenTemp);
    
    return YES;
}

@end
