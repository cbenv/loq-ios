//
//  XQExquisenseButton.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/4/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XQExquisenseButton : UIButton

@property (strong, nonatomic) UIFont *titleFont UI_APPEARANCE_SELECTOR;

@end
