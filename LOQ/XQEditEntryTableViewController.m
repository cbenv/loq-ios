//
//  XQEditEntryTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/4/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQEditEntryTableViewController.h"
#import "XQEditSecondAuthenticationTableViewController.h"
#import "XQScanQRCodeViewController.h"
#import "XQExquisenseSecurity.h"
#import "XQAppDelegate.h"

#define TO_QR_SCANNER_VIEW_CONTROLLER_SEGUE @"toQRScannerViewControllerSegue"
#define TO_EDIT_SECOND_AUTHENTICATION_TABLE_VIEW_CONTROLLER_SEGUE @"toEditSecondAuthenticationTableViewControllerSegue"

@interface XQEditEntryTableViewController () <UITextFieldDelegate, XQEditSecondAuthenticationTableViewControllerDelegate, XQScanQRCodeViewControllerDelegate>
{
    XQAppDelegate *appDelegate;
    
    NSMutableDictionary *entryDetailsTemp;
    
    BOOL enableTwoStepsAuthentication;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveBarButton;

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *idTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;

@property (strong, nonatomic) IBOutlet UITableViewCell *enableTwoStepsAuthenticationCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *configureTwoStepsAuthenticationCell;

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)saveBarButtonPressed:(UIBarButtonItem *)sender;

- (IBAction)generatePasswordButtonPressed:(UIButton *)sender;
- (IBAction)pasteButtonPressed:(UIButton *)sender;

- (IBAction)scanQRCodeButtonPressed:(UIButton *)sender;
- (IBAction)configureManuallyButtonPressed:(UIButton *)sender;

- (IBAction)requiredFieldFilled:(id)sender;

@end

@implementation XQEditEntryTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    [self.nameTextField setDelegate:self];
    [self.idTextField setDelegate:self];
    [self.passwordTextField setDelegate:self];
    [self.passwordTextField setSecureTextEntry:YES];
    [self.nameTextField setReturnKeyType:UIReturnKeyNext];
    [self.idTextField setReturnKeyType:UIReturnKeyNext];
    [self.passwordTextField setReturnKeyType:UIReturnKeyDone];
    
    enableTwoStepsAuthentication = NO;
    
    if (self.entryDetails)
    {
        entryDetailsTemp = [self.entryDetails mutableCopy];
        
        [self.nameTextField setText:[entryDetailsTemp valueForKey:ENTRY_NAME]];
        [self.idTextField setText:[entryDetailsTemp valueForKey:ENTRY_ID]];
        [self.passwordTextField setText:[entryDetailsTemp valueForKey:ENTRY_PASSWORD]];
        
        enableTwoStepsAuthentication = (BOOL) [entryDetailsTemp valueForKey:ENTRY_TOKEN];
    }
    else
    {
        entryDetailsTemp = [[NSMutableDictionary alloc] init];
    }
    
    [self requiredFieldFilled:self];
    
    [self.enableTwoStepsAuthenticationCell setAccessoryType: enableTwoStepsAuthentication ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone];
    [self.configureTwoStepsAuthenticationCell setHidden:!enableTwoStepsAuthentication];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveBarButtonPressed:(UIBarButtonItem *)sender
{
    [entryDetailsTemp setValue:self.nameTextField.text forKey:ENTRY_NAME];
    [entryDetailsTemp setValue:self.idTextField.text forKey:ENTRY_ID];
    [entryDetailsTemp setValue:self.passwordTextField.text forKey:ENTRY_PASSWORD];
    
    XQExquisenseDatabase *database = [appDelegate database];
    
    if ([self.entryDetails objectForKey:ENTRY_NAME])
    {
        [database removeEntryForKey:[self.entryDetails objectForKey:ENTRY_NAME] fromTable:TABLE_NAME];
    }
    
    [database setEntry:entryDetailsTemp forKey:[entryDetailsTemp objectForKey:ENTRY_NAME] toTable:TABLE_NAME];
    
    if (ENABLE_LOGGING) [database printCurrentState];
    
    NSString *hashedPassword = [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
    [appDelegate saveDatabaseWithPassword:hashedPassword];
    
    [self.delegate didEditEntry:entryDetailsTemp];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)generatePasswordButtonPressed:(UIButton *)sender
{
    [self.passwordTextField setText:[XQExquisenseSecurity generatePassword]];
}

- (IBAction)pasteButtonPressed:(UIButton *)sender
{
    [self.passwordTextField setText:[UIPasteboard generalPasteboard].string];
}

- (IBAction)scanQRCodeButtonPressed:(UIButton *)sender
{
    [self performSegueWithIdentifier:TO_QR_SCANNER_VIEW_CONTROLLER_SEGUE sender:self];
}

- (IBAction)configureManuallyButtonPressed:(UIButton *)sender
{
    [self performSegueWithIdentifier:TO_EDIT_SECOND_AUTHENTICATION_TABLE_VIEW_CONTROLLER_SEGUE sender:self];
}

- (IBAction)requiredFieldFilled:(id)sender
{
    // if (!([self.nameTextField.text isEqualToString:@""] ||
    //      [self.idTextField.text isEqualToString:@""] ||
    //      [self.passwordTextField.text isEqualToString:@""]))
    if (![self.nameTextField.text isEqualToString:@""])
    {
        [self.saveBarButton setEnabled:YES];
    }
    else
    {
        [self.saveBarButton setEnabled:NO];
    }
}

#pragma mark - Segue Controllers

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:TO_EDIT_SECOND_AUTHENTICATION_TABLE_VIEW_CONTROLLER_SEGUE] &&
        [segue.destinationViewController isKindOfClass:[XQEditSecondAuthenticationTableViewController class]])
    {
        XQEditSecondAuthenticationTableViewController *editSecondAuthenticationTableViewController = segue.destinationViewController;
        [editSecondAuthenticationTableViewController setDelegate:self];
        [editSecondAuthenticationTableViewController setEntryToken:[entryDetailsTemp valueForKey:ENTRY_TOKEN]];
    }
    if ([segue.identifier isEqualToString:TO_QR_SCANNER_VIEW_CONTROLLER_SEGUE] &&
        [segue.destinationViewController isKindOfClass:[XQScanQRCodeViewController class]])
    {
        XQScanQRCodeViewController *scanQRCodeViewController = segue.destinationViewController;
        [scanQRCodeViewController setDelegate:self];
    }
}

#pragma mark - XQEditSecondAuthenticationTableViewControllerDelegate, XQScanQRCodeViewControllerDelegate

- (void)didConfigure:(NSDictionary *)entryToken
{
    if (entryToken)
    {
        [entryDetailsTemp setValue:entryToken forKey:ENTRY_TOKEN];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Configuration Error"
                                                        message:@"Invalid QR Code."
                                                       delegate:nil
                                              cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.nameTextField])
    {
        [self.idTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.idTextField])
    {
        [self.passwordTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.passwordTextField])
    {
        [self.passwordTextField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            [self.nameTextField becomeFirstResponder];
        }
        else if (indexPath.row == 1)
        {
            [self.idTextField becomeFirstResponder];
        }
        else if (indexPath.row == 2)
        {
            [self.passwordTextField becomeFirstResponder];
        }
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            enableTwoStepsAuthentication = !enableTwoStepsAuthentication;
            [self.enableTwoStepsAuthenticationCell setAccessoryType: enableTwoStepsAuthentication ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone];
            [self.configureTwoStepsAuthenticationCell setHidden:!enableTwoStepsAuthentication];
            
            if (!enableTwoStepsAuthentication)
            {
                [self clearSecondStepAuthenticationInfo];
            }
        }
    }
}

#pragma mark - Helper Methods

- (void)clearSecondStepAuthenticationInfo
{
    [entryDetailsTemp setValue:nil forKey:ENTRY_TOKEN];
}

@end
