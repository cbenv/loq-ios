//
//  XQAboutViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/11/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQAboutViewController.h"

@interface XQAboutViewController ()

@end

@implementation XQAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [XQExquisense addSwipeDownGestureRecognizerToViewController:self
                                                         action:@selector(swipeDownGestureRecognized)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Helper Methods

- (void)swipeDownGestureRecognized
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
