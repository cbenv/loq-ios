//
//  XQVaultSplitViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/12/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XQVaultTableViewController.h"
#import "XQEntryDetailViewController.h"

@protocol XQVaultSplitViewControllerDelegate

@optional

- (void)didSelectEntry:(NSDictionary *)entryDetails;
- (void)didUpdateEntry;

@end

@interface XQVaultSplitViewController : UISplitViewController

@end
