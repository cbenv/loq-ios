//
//  XQExquisenseSecurity.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/6/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XQExquisenseSecurity : NSObject

#define PASSWORD_GENERATOR_SETTINGS_KEY @"Password Generator Settings"
#define PASSWORD_GENERATOR_PASSWORD_LENGTH @"Password Length"
#define PASSWORD_GENERATOR_USE_LOWERCASE_ALPHABETS @"Use lowercase alphabets"
#define PASSWORD_GENERATOR_USE_UPPERCASE_ALPHABETS @"Use uppercase alphabets"
#define PASSWORD_GENERATOR_USE_NUMBERS @"Use numbers"
#define PASSWORD_GENERATOR_USE_SPECIAL_CHARACTERS @"Use special characters"
#define PASSWORD_GENERATOR_USE_AMBIGUOUS_CHARACTERS @"Use ambiguous characters"

+ (NSString *)SHA1OfString:(NSString *)string;
+ (NSString *)SHA256OfString:(NSString *)string;
+ (NSString *)SHA512OfString:(NSString *)string;

+ (NSString *)generatePassword;

+ (NSData *)AESEncryptOfData:(NSData *)data withKey:(NSString *)key;
+ (NSData *)AESDecryptOfData:(NSData *)data withKey:(NSString *)key;

@end
