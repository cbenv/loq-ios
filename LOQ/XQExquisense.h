//
//  XQExquisense.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/3/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XQExquisense : NSObject

#pragma mark - Interface

+ (void)applyStyle;
+ (UIColor *)exquisenseOrange;
+ (UIColor *)exquisenseOrangeComplement;

#pragma mark - Interaction

+ (void)addSwipeDownGestureRecognizerToViewController:(UIViewController *)viewController action:(SEL)selector;

@end
