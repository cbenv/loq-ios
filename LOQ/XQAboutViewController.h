//
//  XQAboutViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/11/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseViewController.h"

@interface XQAboutViewController : XQExquisenseViewController

@end
