//
//  XQLogOnViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/3/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQLogOnViewController.h"
#import "XQExquisenseButton.h"
#import "XQExquisenseSecurity.h"
#import "XQAppDelegate.h"

#define TO_TAB_BAR_CONTROLLER_SEGUE @"toTabBarControllerSegue"

@interface XQLogOnViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *masterPasswordTextField;
@property (strong, nonatomic) IBOutlet XQExquisenseButton *accessVaultButton;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)accessVaultButtonPressed:(id)sender;
- (IBAction)requiredFieldFilled:(UITextField *)sender;

@end

@implementation XQLogOnViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [self.accessVaultButton setTitleFont:[UIFont boldSystemFontOfSize:28]];
        
        [self.masterPasswordTextField removeFromSuperview];
        [self.accessVaultButton removeFromSuperview];
        
        if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))
        {
            [self.imageView setAlpha:0];
            
            [self.masterPasswordTextField setFrame:CGRectMake(312, 137, 400, 30)];
            [self.accessVaultButton setFrame:CGRectMake(232, 176, 560, 100)];
        }
        else
        {
            [self.imageView setAlpha:1];
            
            [self.masterPasswordTextField setFrame:CGRectMake(184, 585, 400, 30)];
            [self.accessVaultButton setFrame:CGRectMake(104, 624, 560, 100)];
        }
        
        [self.view addSubview:self.masterPasswordTextField];
        [self.view addSubview:self.accessVaultButton];
    }
    
    [self.accessVaultButton setTitleColor:[XQExquisense exquisenseOrangeComplement] forState:UIControlStateNormal];
    [self.accessVaultButton setEnabled:NO];
    [self.masterPasswordTextField setSecureTextEntry:YES];
    [self.masterPasswordTextField setDelegate:self];
    [self.masterPasswordTextField setReturnKeyType:UIReturnKeyGo];
    
    //[XQExquisense addSwipeDownGestureRecognizerToViewController:self action:@selector(swipeDownGestureRecognized)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
        
    [self.masterPasswordTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [self.accessVaultButton setTitleFont:[UIFont boldSystemFontOfSize:28]];
        
        [self.masterPasswordTextField removeFromSuperview];
        [self.accessVaultButton removeFromSuperview];
        
        if (UIDeviceOrientationIsLandscape(toInterfaceOrientation))
        {
            [self.imageView setAlpha:0];
            
            [self.masterPasswordTextField setFrame:CGRectMake(312, 137, 400, 30)];
            [self.accessVaultButton setFrame:CGRectMake(232, 176, 560, 100)];
        }
        else
        {
            [self.imageView setAlpha:1];
            
            [self.masterPasswordTextField setFrame:CGRectMake(184, 585, 400, 30)];
            [self.accessVaultButton setFrame:CGRectMake(104, 624, 560, 100)];
        }
        
        [self.view addSubview:self.masterPasswordTextField];
        [self.view addSubview:self.accessVaultButton];
        
        [self.masterPasswordTextField becomeFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.masterPasswordTextField])
    {
        [self accessVaultButtonPressed:textField];
    }
    
    return YES;
}

#pragma mark - IBActions

- (IBAction)accessVaultButtonPressed:(id)sender
{
    NSString *hashedPassword = [XQExquisenseSecurity SHA512OfString:self.masterPasswordTextField.text];
    NSString *masterCredentials = (NSString *) [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
    
    if (![hashedPassword isEqualToString:masterCredentials])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Password"
                                                        message:@"Please make sure to type the password correctly."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([hashedPassword isEqualToString:masterCredentials])
    {
        NSString *hashedPassword = [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
        [(XQAppDelegate *)[[UIApplication sharedApplication] delegate] loadDatabaseWithPassword:hashedPassword];
        
        [self.masterPasswordTextField resignFirstResponder];
        
        [self performSegueWithIdentifier:TO_TAB_BAR_CONTROLLER_SEGUE sender:self];
    }
}

- (IBAction)requiredFieldFilled:(UITextField *)sender
{
    if (![self.masterPasswordTextField.text isEqualToString:@""])
    {
        [self.accessVaultButton setEnabled:YES];
    }
    else
    {
        [self.accessVaultButton setEnabled:NO];
    }
}

#pragma mark - Selector Methods

- (void)swipeDownGestureRecognized
{
    [self.masterPasswordTextField resignFirstResponder];
}

@end
