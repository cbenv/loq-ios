//
//  XQVaultTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/7/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQVaultTableViewController.h"
#import "XQAppDelegate.h"
#import "XQEntryDetailViewController.h"
#import "XQEditEntryTableViewController.h"

#define TO_ENTRY_DETAIL_VIEW_CONTROLLER_SEGUE @"toEntryDetailViewControllerSegue"
#define TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE @"toEditEntryTableViewControllerSegue"

@interface XQVaultTableViewController () <XQVaultSplitViewControllerDelegate>
{
    XQExquisenseDatabase *database;
    NSArray *entries;
}

@end

@implementation XQVaultTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    database = [(XQAppDelegate *)[[UIApplication sharedApplication] delegate] database];
    
    if (ENABLE_LOGGING) [database printCurrentState];
    
    entries = [database getEntriesFromTable:TABLE_NAME];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [entries count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"entryCell"];
    
    [cell.textLabel setText:[[entries objectAtIndex:indexPath.row] objectForKey:ENTRY_NAME]];
    [cell.textLabel setTextColor:[XQExquisense exquisenseOrange]];
    
    return cell;
}

#pragma mark - Segue Controllers

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:TO_ENTRY_DETAIL_VIEW_CONTROLLER_SEGUE] &&
        [segue.destinationViewController isKindOfClass:[XQEntryDetailViewController class]])
    {
        XQEntryDetailViewController *entryDetailViewController = segue.destinationViewController;

        NSDictionary *entryDetails = [entries objectAtIndex:[(NSIndexPath *)sender row]];
        
        [entryDetailViewController setEntryDetails:entryDetails];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [self.delegate didSelectEntry:[entries objectAtIndex:indexPath.row]];
    }
    else
    {
        [self performSegueWithIdentifier:TO_ENTRY_DETAIL_VIEW_CONTROLLER_SEGUE sender:indexPath];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.0;
}

#pragma mark - IBActions

- (IBAction)addBarButtonPressed:(UIBarButtonItem *)sender
{
    [self performSegueWithIdentifier:TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE sender:self];
}

#pragma mark - XQVaultSplitViewControllerDelegate

- (void)didUpdateEntry
{
    entries = [database getEntriesFromTable:TABLE_NAME];
    [self.tableView reloadData];
}

@end
