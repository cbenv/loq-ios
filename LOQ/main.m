//
//  main.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/3/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XQAppDelegate class]));
    }
}
