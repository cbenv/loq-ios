//
//  XQEditEntryAndSecondAuthenticationTableViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/13/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseTableViewController.h"

@protocol XQEditEntryAndSecondAuthenticationTableViewControllerDelegate

@required

- (void)didEditEntry:(NSDictionary *)entryDetails;

@end

@interface XQEditEntryAndSecondAuthenticationTableViewController : XQExquisenseTableViewController

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSDictionary *entryDetails;

@end
