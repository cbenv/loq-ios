//
//  XQEntryDetailViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/10/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseViewController.h"

@interface XQEntryDetailViewController : XQExquisenseViewController

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSDictionary *entryDetails;

- (IBAction)editBarButtonPressed:(UIBarButtonItem *)sender;

@end
