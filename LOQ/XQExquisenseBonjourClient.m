//
//  XQExquisenseBonjourClient.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/22/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseBonjourClient.h"

@interface XQExquisenseBonjourClient () <NSNetServiceBrowserDelegate, NSNetServiceDelegate, NSStreamDelegate>
{
    NSNetServiceBrowser *netServiceBrowser;
    NSNetService *netService; // announcing service;
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    NSString *netServiceType;
    NSString *netServiceTransportLayer;
    NSString *netServiceDomain;
    NSString *netServiceName;
    uint16_t netServicePort;
    uint8_t netServicePayloadSize;
    
    NSMutableArray *broadcastingDevices;
}

@end

@implementation XQExquisenseBonjourClient

- (id)init
{
    self = [super init];
    if (self)
    {
        self = [self initWithServiceType:@"server"];
    }
    return self;
}

- (id)initWithServiceType:(NSString *)type
{
    self = [super init];
    if (self)
    {
        self = [self initWithServiceType:type inDomain:@""];
    }
    return self;
}

- (id)initWithServiceType:(NSString *)type inDomain:(NSString *)domain
{
    self = [super init];
    if (self)
    {
        netServiceType = type;
        netServiceDomain = domain;
        netServiceTransportLayer = @"tcp";
        netServicePayloadSize = 128;
    }
    return self;
}

- (void)startBrowsing
{
    if (broadcastingDevices)
    {
        [broadcastingDevices removeAllObjects];
    }
    else
    {
        broadcastingDevices = [NSMutableArray array];
    }
    
    NSString *serviceType = [NSString stringWithFormat:@"_%@._%@.", netServiceType, netServiceTransportLayer];
    
    netServiceBrowser = [[NSNetServiceBrowser alloc] init];
    [netServiceBrowser setDelegate:self];
    [netServiceBrowser scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [netServiceBrowser searchForServicesOfType:serviceType inDomain:netServiceDomain];
}

- (void)stop
{
    [self stopStream];
    [self stopService];
}

- (void)setNetService:(NSNetService *)service
{
    [service setDelegate:self];
    [service resolveWithTimeout:0.0];
    
    [self stopBrowser];
}

- (NSString *)getPairedDeviceName
{
    return (inputStream && outputStream) ? [netService name] : nil;
}

- (void)sendData:(NSData *)data
{
    [outputStream write:(const uint8_t *)[data bytes] maxLength:[data length]];
}

#pragma mark - NSNetServiceBrowserDelegate

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindDomain:(NSString *)domainString moreComing:(BOOL)moreComing
{
    
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveDomain:(NSString *)domainString moreComing:(BOOL)moreComing
{
    
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing
{
    if (aNetService)
    {
        [broadcastingDevices addObject:aNetService];
    }
    if (!moreComing)
    {
        [self.delegate didRefreshBroadcastingDevices:broadcastingDevices];
    }
}

- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing
{
    if (aNetService)
    {
        [broadcastingDevices removeObject:aNetService];
    }
    if (!moreComing)
    {
        [self.delegate didRefreshBroadcastingDevices:broadcastingDevices];
    }
}

- (void)netServiceBrowserWillSearch:(NSNetServiceBrowser *)aNetServiceBrowser
{
    
}

- (void)netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)aNetServiceBrowser
{
    
}

#pragma mark - NSNetServiceDelegate

- (void)netServiceWillPublish:(NSNetService *)sender
{
    
}

- (void)netService:(NSNetService *)sender didNotPublish:(NSDictionary *)errorDict
{
    
}

- (void)netServiceDidPublish:(NSNetService *)sender
{
    
}

- (void)netServiceWillResolve:(NSNetService *)sender
{
    
}

- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict
{
    
}

- (void)netServiceDidResolveAddress:(NSNetService *)sender
{
    NSLog(@"Connected to %@ of type %@ in domain %@ on port %li", [sender name], [sender type], [sender domain], (long)[sender port]);
    
    netService = sender;
    
    NSInputStream *iStream = nil;
    NSOutputStream *oStream = nil;
    
    if ([netService getInputStream:&iStream outputStream:&oStream])
    {
        [self connectInputStream:iStream outputStream:oStream];
    }
    
    [self.delegate didConnect];
}

- (void)netServiceDidStop:(NSNetService *)sender
{
    netService = nil;
    
    [self.delegate didDisconnect];
}

#pragma mark - NSStreamDelegate

- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)eventCode
{
    if (eventCode == NSStreamEventEndEncountered)
    {
        [self stopStream];
        
        [self.delegate didDisconnect];
    }
    else if (eventCode == NSStreamEventErrorOccurred)
    {
        [self stopService];
        [self stopStream];
        
        [self.delegate didDisconnect];
    }
    else if (eventCode == NSStreamEventHasBytesAvailable)
    {
        NSData *data = [self processData:aStream];
        
        if (data)
        {
            [self.delegate didReceiveData:data];
        }
    }
    else if (eventCode == NSStreamEventHasSpaceAvailable)
    {
        
    }
    else if (eventCode == NSStreamEventOpenCompleted)
    {
        // [self stopService];
        
        [self.delegate didConnect];
    }
    else if (eventCode == NSStreamEventNone)
    {
        
    }
}

#pragma mark - Helper Methods

- (NSData *)processData:(NSStream *)stream
{
    NSMutableData *data = [NSMutableData data];
    uint8_t *buffer = calloc(netServicePayloadSize, sizeof(uint8_t));
    NSUInteger length = 0;
    NSInputStream *iStream = (NSInputStream *)stream;
    
    while([iStream hasBytesAvailable])
    {
        length = [iStream read:buffer maxLength:netServicePayloadSize];
        if (length > 0) [data appendBytes:buffer length:length];
    }
    
    free(buffer);
    
    return data;
}

- (void)connectInputStream:(NSInputStream *)iStream outputStream:(NSOutputStream *)oStream
{
    inputStream = iStream;
    outputStream = oStream;
    
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    [inputStream open];
    [outputStream open];
}

- (void)stopService
{
    if (netService)
    {
        [netService stop];
        [netService removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        netService = nil;
    }
}

- (void)stopStream
{
    if (inputStream)
    {
        [inputStream close];
        [inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        inputStream = nil;
    }
    if (outputStream)
    {
        [outputStream close];
        [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        outputStream = nil;
    }
}

- (void)stopBrowser
{
    if (netServiceBrowser)
    {
        [netServiceBrowser stop];
        [netServiceBrowser removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        netServiceBrowser = nil;
    }
}

@end
