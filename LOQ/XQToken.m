//
//  XQToken.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/10/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//
//  Adapted from FreeOTP by Nathaniel McCallum <npmccallum@redhat.com> with several modifications.
//  In compliance with Apache License v2.0, as required by licensor.
//  Copyright (C) 2013 Nathaniel McCallum, RedHat.
//

#import "XQToken.h"

#import <CommonCrypto/CommonHMAC.h>
#import "base32.h"
#import <sys/time.h>

@implementation XQToken
{
    CCHmacAlgorithm algorithm;
    NSData *secret;
    NSUInteger digits;
    uint32_t interval;
    uint64_t counter;
    
    NSString *tokenType;
    NSString *keyType;
}

#pragma mark - Static Methods

static inline size_t getDigestLength(CCHmacAlgorithm algorithm)
{
    switch (algorithm)
    {
        case kCCHmacAlgMD5:
            return CC_MD5_DIGEST_LENGTH;
        case kCCHmacAlgSHA1:
            return CC_SHA1_DIGEST_LENGTH;
        case kCCHmacAlgSHA256:
            return CC_SHA256_DIGEST_LENGTH;
        case kCCHmacAlgSHA512:
            return CC_SHA512_DIGEST_LENGTH;
            
        default:
            return CC_SHA1_DIGEST_LENGTH;
    }
}

static NSData* parseBase32Key(const NSString *secret)
{
    uint8_t key[4096];
    
    if (secret == nil)
    {
        return nil;
    }
    const char *tmp = [secret cStringUsingEncoding:NSASCIIStringEncoding];
    if (tmp == NULL)
    {
        return nil;
    }
    
    int res = base32_decode(tmp, key, sizeof(key));
    if (res < 0 || res == sizeof(key))
    {
        return nil;
    }
    
    return [NSData dataWithBytes:key length:res];
}

static NSData *parseHexKey(NSString *hex)
{
    NSArray *hexCharSet = [@"0 1 2 3 4 5 6 7 8 9 A B C D E F" componentsSeparatedByString:@" "];
    
    hex = [hex uppercaseString];
    
    unsigned long n = [hex length];
    
    NSMutableString *temp = [[NSMutableString alloc] init];
    
    for (int i = 0; i < n; i++)
    {
        [temp appendFormat:@"%c", [hex characterAtIndex:i]];
        if (i % 2 == 1) [temp appendString:@" "];
    }
    
    NSArray *hexChars = [temp componentsSeparatedByString:@" "];
    
    temp = [[NSMutableString alloc] init];
    
    for (NSString *hexChar in hexChars)
    {
        if ([hexChar length] == 0) break;
        
        int sum = 0;
        
        for (int i = 0; i <= 1; i++)
        {
            unsigned long index = [hexCharSet indexOfObject:[NSString stringWithFormat:@"%c", [hexChar characterAtIndex:i]]];
            sum = sum + index * pow(16, 1 - i);
        }
        
        [temp appendFormat:@"%c", sum];
    }
    
    return [temp dataUsingEncoding:NSUTF8StringEncoding];
}

static CCHmacAlgorithm parseAlgorithm(const NSString* algorithm)
{
    if ([algorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_MD5])
    {
        return kCCHmacAlgMD5;
    }
    else if ([algorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA1])
    {
        return kCCHmacAlgSHA1;
    }
    else if ([algorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA256])
    {
        return kCCHmacAlgSHA256;
    }
    else if ([algorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA512])
    {
        return kCCHmacAlgSHA512;
    }
    else
    {
        return kCCHmacAlgSHA1;
    }
}

static NSString* getHOTP(CCHmacAlgorithm algorithm, uint8_t digits, NSData* key, uint64_t counter)
{
#ifdef __LITTLE_ENDIAN__
    // Network byte order
    counter = (((uint64_t) htonl(counter)) << 32) + htonl(counter >> 32);
#endif
    
    // Create digits divisor
    uint32_t div = 1;
    for (int i = digits; i > 0; i--)
        div *= 10;
    
    // Create the HMAC
    uint8_t digest[getDigestLength(algorithm)];
    CCHmac(algorithm, [key bytes], [key length], &counter, sizeof(counter), digest);
    
    // Truncate
    uint32_t binary;
    uint32_t off = digest[sizeof(digest) - 1] & 0xf;
    binary  = (digest[off + 0] & 0x7f) << 0x18;
    binary |= (digest[off + 1] & 0xff) << 0x10;
    binary |= (digest[off + 2] & 0xff) << 0x08;
    binary |= (digest[off + 3] & 0xff) << 0x00;
    binary  = binary % div;
    
    return [NSString stringWithFormat:[NSString stringWithFormat:@"%%0%hhulu", digits], binary];
}

#pragma mark - Initializers

- (id)initWithTokenDetails:(NSDictionary *)tokenDetails
{
    tokenType = [tokenDetails objectForKey:ENTRY_TOKEN_TYPE];
    keyType = [tokenDetails objectForKey:ENTRY_TOKEN_SECRET_TYPE];
    
    algorithm = parseAlgorithm([tokenDetails objectForKey:ENTRY_TOKEN_ALGORITHM]);
    digits = [[tokenDetails objectForKey:ENTRY_TOKEN_DIGITS] unsignedIntValue];
    interval = [[tokenDetails objectForKey:ENTRY_TOKEN_INTERVAL] unsignedIntValue];
    counter = [[tokenDetails objectForKey:ENTRY_TOKEN_COUNTER] unsignedLongLongValue];
    
    if ([keyType isEqualToString:ENTRY_TOKEN_SECRET_TYPE_HEX])
    {
        secret = parseHexKey([tokenDetails objectForKey:ENTRY_TOKEN_SECRET]);
    }
    else // if ([keyType isEqualToString:ENTRY_TOKEN_SECRET_TYPE_BASE32])
    {
        secret = parseBase32Key([tokenDetails objectForKey:ENTRY_TOKEN_SECRET]);
    }
    
    return self;
}

#pragma mark - Getter and Setter Methods

- (XQTokenCode *)tokenCode
{
    time_t now = time(NULL);
    if (now == (time_t) - 1)
    {
        now = 0;
    }
    
    if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_HOTP])
    {
        NSString* code = getHOTP(algorithm, digits, secret, counter++);

        return [[XQTokenCode alloc] initWithCode:code startTime:now endTime:(now + 30)];
    }
    else // if ([type isEqualToString:ENTRY_AUTHENTICATION_TYPE_TOTP])
    {
        XQTokenCode* next = [[XQTokenCode alloc] initWithCode:getHOTP(algorithm, digits, secret, now / interval + 1)
                                                    startTime:now / interval * interval + interval
                                                      endTime:now / interval * interval + interval + interval];
        
        return [[XQTokenCode alloc] initWithCode:getHOTP(algorithm, digits, secret, now / interval)
                                       startTime:now / interval * interval
                                         endTime:now / interval * interval + interval
                                   nextTokenCode:next];
    }
}

@end