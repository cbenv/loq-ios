//
//  XQEditEntryTableViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/4/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseTableViewController.h"

@protocol XQEditEntryTableViewControllerDelegate

@optional

- (void)didEditEntry:(NSDictionary *)entryDetails;

@end

@interface XQEditEntryTableViewController : XQExquisenseTableViewController

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSDictionary *entryDetails;

@end
