//
//  XQSignUpViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/3/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQSignUpViewController.h"
#import "XQExquisenseButton.h"
#import "XQExquisenseSecurity.h"
#import "XQAppDelegate.h"

#define TO_TAB_BAR_CONTROLLER_SEGUE @"toTabBarControllerSegue"

@interface XQSignUpViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *masterPasswordTextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (strong, nonatomic) IBOutlet XQExquisenseButton *saveMasterPasswordButton;
@property (strong, nonatomic) IBOutlet XQExquisenseButton *configureMasterPasswordButton;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)configureMasterPasswordButtonPressed:(UIButton *)sender;
- (IBAction)saveMasterPasswordButtonPressed:(id)sender;
- (IBAction)requiredFieldFilled:(UITextField *)sender;

@end

@implementation XQSignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [self.saveMasterPasswordButton setTitleFont:[UIFont boldSystemFontOfSize:28]];
        [self.configureMasterPasswordButton setTitleFont:[UIFont boldSystemFontOfSize:28]];
        
        [self.masterPasswordTextField removeFromSuperview];
        [self.confirmPasswordTextField removeFromSuperview];
        [self.saveMasterPasswordButton removeFromSuperview];
        
        if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))
        {
            [self.masterPasswordTextField setFrame:CGRectMake(312, 119, 400, 30)];
            [self.confirmPasswordTextField setFrame:CGRectMake(312, 158, 400, 30)];
            [self.saveMasterPasswordButton setFrame:CGRectMake(232, 196, 560, 100)];
        }
        else
        {
            [self.masterPasswordTextField setFrame:CGRectMake(184, 585, 400, 30)];
            [self.confirmPasswordTextField setFrame:CGRectMake(184, 624, 400, 30)];
            [self.saveMasterPasswordButton setFrame:CGRectMake(104, 662, 560, 100)];
        }
        
        [self.view addSubview:self.masterPasswordTextField];
        [self.view addSubview:self.confirmPasswordTextField];
        [self.view addSubview:self.saveMasterPasswordButton];
    }
    
    [self.saveMasterPasswordButton setTitleColor:[XQExquisense exquisenseOrangeComplement]
                                        forState:UIControlStateNormal];
    [self.configureMasterPasswordButton setTitleColor:[XQExquisense exquisenseOrangeComplement]
                                             forState:UIControlStateNormal];
    
    [self.masterPasswordTextField setAlpha:0];
    [self.confirmPasswordTextField setAlpha:0];
    [self.saveMasterPasswordButton setAlpha:0];
    [self.saveMasterPasswordButton setEnabled:NO];
    [self.masterPasswordTextField setSecureTextEntry:YES];
    [self.confirmPasswordTextField setSecureTextEntry:YES];
    [self.masterPasswordTextField setDelegate:self];
    [self.confirmPasswordTextField setDelegate:self];
    [self.masterPasswordTextField setReturnKeyType:UIReturnKeyNext];
    [self.confirmPasswordTextField setReturnKeyType:UIReturnKeyGo];
    
    [XQExquisense addSwipeDownGestureRecognizerToViewController:self
                                                         action:@selector(swipeDownGestureRecognized)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [self.masterPasswordTextField removeFromSuperview];
        [self.confirmPasswordTextField removeFromSuperview];
        [self.saveMasterPasswordButton removeFromSuperview];
        
        if (UIDeviceOrientationIsLandscape(toInterfaceOrientation))
        {
            [self.masterPasswordTextField setFrame:CGRectMake(312, 119, 400, 30)];
            [self.confirmPasswordTextField setFrame:CGRectMake(312, 158, 400, 30)];
            [self.saveMasterPasswordButton setFrame:CGRectMake(232, 196, 560, 100)];
            
            if (self.saveMasterPasswordButton.alpha == 1)
            {
                [self.imageView setAlpha:0];
                [self.masterPasswordTextField becomeFirstResponder];
            }
        }
        else
        {
            [self.masterPasswordTextField setFrame:CGRectMake(184, 585, 400, 30)];
            [self.confirmPasswordTextField setFrame:CGRectMake(184, 624, 400, 30)];
            [self.saveMasterPasswordButton setFrame:CGRectMake(104, 662, 560, 100)];
            
            if (self.saveMasterPasswordButton.alpha == 1)
            {
                [self.imageView setAlpha:1];
                [self.masterPasswordTextField becomeFirstResponder];
            }
        }
        
        [self.view addSubview:self.masterPasswordTextField];
        [self.view addSubview:self.confirmPasswordTextField];
        [self.view addSubview:self.saveMasterPasswordButton];
        
        if (self.saveMasterPasswordButton.alpha == 1)
        {
            [self.masterPasswordTextField becomeFirstResponder];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.masterPasswordTextField])
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.confirmPasswordTextField])
    {
        [self saveMasterPasswordButtonPressed:textField];
    }
    
    return YES;
}

#pragma mark - IBActions

- (IBAction)configureMasterPasswordButtonPressed:(UIButton *)sender
{
    [UIView animateWithDuration:1 animations:^
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            [self.imageView setAlpha:0];
        }
        [self.configureMasterPasswordButton setAlpha:0];
    }];
    
    [UIView animateWithDuration:1 animations:^
    {
        [self.masterPasswordTextField setAlpha:1];
        [self.confirmPasswordTextField setAlpha:1];
        [self.saveMasterPasswordButton setAlpha:1];
    }
    completion:^(BOOL finished)
    {
        [self.masterPasswordTextField becomeFirstResponder];
    }];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))
        {
            [self.imageView setAlpha:0];
        }
        else
        {
            [self.imageView setAlpha:1];
        }
    }
}

- (IBAction)saveMasterPasswordButtonPressed:(id)sender
{
    if (![self.masterPasswordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Mismatch"
                                                        message:@"Please confirm your password."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if ([self.masterPasswordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        NSString *hashedPassword = [XQExquisenseSecurity SHA512OfString:self.masterPasswordTextField.text];
        
        [(XQAppDelegate *)[[UIApplication sharedApplication] delegate] saveDatabaseWithPassword:hashedPassword];
        
        [[NSUserDefaults standardUserDefaults] setObject:hashedPassword forKey:MASTER_CREDENTIALS_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Configuration Successful"
                                                        message:@"You have successfully set a master password. Please remember and use this password the next time you try to access this application."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        [self.confirmPasswordTextField resignFirstResponder];
        
        [self performSegueWithIdentifier:TO_TAB_BAR_CONTROLLER_SEGUE sender:self];
    }
}

- (IBAction)requiredFieldFilled:(UITextField *)sender
{
    if (!([self.masterPasswordTextField.text isEqualToString:@""] ||
          [self.confirmPasswordTextField.text isEqualToString:@""]))
    {
        [self.saveMasterPasswordButton setEnabled:YES];
    }
    else
    {
        [self.saveMasterPasswordButton setEnabled:NO];
    }
}

#pragma mark - Selector Methods

- (void)swipeDownGestureRecognized
{
    [self.masterPasswordTextField resignFirstResponder];
    [self.confirmPasswordTextField resignFirstResponder];
    
    [UIView animateWithDuration:1 animations:^
    {
        [self.masterPasswordTextField setAlpha:0];
        [self.confirmPasswordTextField setAlpha:0];
        [self.saveMasterPasswordButton setAlpha:0];
    }];
    
    [UIView animateWithDuration:1 animations:^
    {
        [self.imageView setAlpha:1];
        [self.configureMasterPasswordButton setAlpha:1];
    }];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        if (UIDeviceOrientationIsLandscape(self.interfaceOrientation))
        {
            [self.imageView setAlpha:1];
        }
        else
        {
            [self.imageView setAlpha:1];
        }
    }
}

@end
