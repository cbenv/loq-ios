//
//  XQPasswordGeneratorLengthViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/10/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ENTRY_PASSWORD_LENGTH_DEFAULT 12
#define ENTRY_PASSWORD_LENGTH_MINIMUM 4
#define ENTRY_PASSWORD_LENGTH_MAXIMUM 128

@interface XQPasswordGeneratorLengthViewController : UIViewController

@property (weak, nonatomic) id delegate;

- (void)setPasswordLength:(int)passwordLength;
- (int)getPasswordLength;

@end
