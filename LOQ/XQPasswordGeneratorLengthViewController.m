//
//  XQPasswordGeneratorLengthViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/10/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQPasswordGeneratorLengthViewController.h"
#import "XQExquisenseSecurity.h"

@interface XQPasswordGeneratorLengthViewController ()
{
    int passwordLengthTemp;
}

@property (strong, nonatomic) IBOutlet UIView *passwordLengthCell;
@property (strong, nonatomic) IBOutlet UILabel *passwordLengthLabel;
@property (strong, nonatomic) IBOutlet UISlider *passwordLengthSlider;
@property (strong, nonatomic) IBOutlet UIStepper *passwordLengthStepper;

- (IBAction)passwordLengthChanged:(id)sender;

@end

@implementation XQPasswordGeneratorLengthViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.passwordLengthLabel setTextColor:[XQExquisense exquisenseOrange]];
    [self.passwordLengthLabel setFont:[UIFont boldSystemFontOfSize:18]];
    
    [self.passwordLengthSlider setMinimumValue:ENTRY_PASSWORD_LENGTH_MINIMUM];
    [self.passwordLengthSlider setMaximumValue:ENTRY_PASSWORD_LENGTH_MAXIMUM];
    [self.passwordLengthStepper setMinimumValue:ENTRY_PASSWORD_LENGTH_MINIMUM];
    [self.passwordLengthStepper setMaximumValue:ENTRY_PASSWORD_LENGTH_MAXIMUM];
    
    passwordLengthTemp = [[[[NSUserDefaults standardUserDefaults] dictionaryForKey:PASSWORD_GENERATOR_SETTINGS_KEY] valueForKey:PASSWORD_GENERATOR_PASSWORD_LENGTH] intValue];
    if (!passwordLengthTemp)
    {
        passwordLengthTemp = ENTRY_PASSWORD_LENGTH_DEFAULT;
    }
    
    [self setPasswordLength:passwordLengthTemp];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    NSLog(@"PasswordGeneratorLengthViewController: %f", self.view.frame.size.width);
    
    [self.passwordLengthCell setFrame:CGRectMake(0, 49, self.view.frame.size.width, 44)];
    
    NSLog(@"PasswordLengthCell: %f", self.passwordLengthCell.frame.size.width);
}

- (IBAction)passwordLengthChanged:(id)sender
{
    if ([sender isKindOfClass:[UISlider class]])
    {
        [self setPasswordLength:[(UISlider *)sender value]];
    }
    else if ([sender isKindOfClass:[UIStepper class]])
    {
        [self setPasswordLength:[(UIStepper *)sender value]];
    }
}

- (void)setPasswordLength:(int)passwordLength
{
    passwordLengthTemp = passwordLength;
    [self.passwordLengthLabel setText:[NSString stringWithFormat:@"PASSWORD LENGTH: %i", passwordLength]];
    [self.passwordLengthSlider setValue:passwordLength];
    [self.passwordLengthStepper setValue:passwordLength];
}

- (int)getPasswordLength
{
    return passwordLengthTemp;
}

@end
