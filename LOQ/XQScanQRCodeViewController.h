//
//  XQScanQRCodeViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/6/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//
//  Adapted from FreeOTP by Nathaniel McCallum <npmccallum@redhat.com> with several modifications.
//  In compliance with Apache License v2.0, as required by licensor.
//  Copyright (C) 2013 Nathaniel McCallum, RedHat.
//

#import "XQExquisenseViewController.h"

@protocol XQScanQRCodeViewControllerDelegate

@required

- (void)didConfigure:(NSDictionary *)entryToken;

@end

@interface XQScanQRCodeViewController : XQExquisenseViewController

@property (weak, nonatomic) id delegate;

@end
