//
//  XQEditSecondAuthenticationTableViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/5/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseTableViewController.h"

@protocol XQEditSecondAuthenticationTableViewControllerDelegate

@required

- (void)didConfigure:(NSDictionary *)entryToken;

@end

@interface XQEditSecondAuthenticationTableViewController : XQExquisenseTableViewController

@property (weak, nonatomic) id delegate;

@property (strong, nonatomic) NSDictionary *entryToken;

@end
