//
//  XQExquisenseViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/3/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XQExquisenseViewController : UIViewController

- (UIStatusBarStyle)preferredStatusBarStyle;

@end
