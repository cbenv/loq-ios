//
//  XQEntryDetailViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/10/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQEntryDetailViewController.h"
#import "XQEditEntryTableViewController.h"
#import "XQEditEntryAndSecondAuthenticationTableViewController.h"
#import "XQAppDelegate.h"
#import "XQExquisenseButton.h"
#import "XQToken.h"
#import "XQVaultSplitViewController.h"
#import "XQVaultTableViewController.h"
#import "XQExquisenseBonjourClient.h"

#define TIMER_INTERVAL 0.05

#define TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE @"toEditEntryTableViewControllerSegue"

@interface XQEntryDetailViewController () <UIAlertViewDelegate, XQEditEntryTableViewControllerDelegate, XQVaultSplitViewControllerDelegate, UISplitViewControllerDelegate, UIScrollViewDelegate>
{
    NSTimer *timer;
    XQToken *token;
    
    XQAppDelegate *appDelegate;
    
    XQExquisenseBonjourClient *client;
}

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *connectionLabel;
@property (strong, nonatomic) IBOutlet UILabel *tokenLabel;
@property (strong, nonatomic) IBOutlet UIButton *refreshButton;
@property (strong, nonatomic) IBOutlet UIProgressView *tokenProgressView;
@property (strong, nonatomic) IBOutlet UIImageView *usernameLogo;
@property (strong, nonatomic) IBOutlet UIImageView *passwordLogo;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UILabel *tokenCopiedToClipboardLabel;
@property (strong, nonatomic) IBOutlet UILabel *usernameCopiedToClipboardLabel;
@property (strong, nonatomic) IBOutlet UILabel *passwordCopiedToClipboardLabel;

@property (strong, nonatomic) IBOutlet UIButton *usernameCopyButton;
@property (strong, nonatomic) IBOutlet UIButton *passwordCopyButton;
@property (strong, nonatomic) IBOutlet UIButton *tokenCopyButton;
@property (strong, nonatomic) IBOutlet XQExquisenseButton *deleteEntryButton;

- (IBAction)backBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)refreshTokenButtonPressed:(UIButton *)sender;
- (IBAction)copyUsernameButtonPressed:(UIButton *)sender;
- (IBAction)copyPasswordButtonPressed:(UIButton *)sender;
- (IBAction)copyTokenButtonPressed:(UIButton *)sender;
- (IBAction)deleteEntryButtonPressed:(UIButton *)sender;

@end

@implementation XQEntryDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    client = [appDelegate client];
    
    [self.nameLabel setFont:[UIFont boldSystemFontOfSize:28]];
    [self.tokenLabel setFont:[UIFont boldSystemFontOfSize:52]];
    [self.usernameLabel setFont:[UIFont systemFontOfSize:25]];
    [self.passwordLabel setFont:[UIFont systemFontOfSize:25]];
    // [self.deleteEntryButton setTitleFont:[UIFont boldSystemFontOfSize:25]];
    [self.usernameCopiedToClipboardLabel setFont:[UIFont boldSystemFontOfSize:12]];
    [self.passwordCopiedToClipboardLabel setFont:[UIFont boldSystemFontOfSize:12]];
    [self.tokenCopiedToClipboardLabel setFont:[UIFont boldSystemFontOfSize:12]];
    [self.connectionLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    [self.nameLabel setTextColor:[XQExquisense exquisenseOrange]];
    [self.tokenLabel setTextColor:[XQExquisense exquisenseOrange]];
    [self.usernameLabel setTextColor:[XQExquisense exquisenseOrange]];
    [self.passwordLabel setTextColor:[XQExquisense exquisenseOrange]];
    [self.tokenProgressView setProgressTintColor:[XQExquisense exquisenseOrange]];
    [self.tokenProgressView setTrackTintColor:[UIColor clearColor]];
    // [self.deleteEntryButton setTitleColor:[XQExquisense exquisenseOrange] forState:UIControlStateNormal];
    [self.connectionLabel setTextColor:[XQExquisense exquisenseOrange]];
    [self.usernameCopiedToClipboardLabel setTextColor:[XQExquisense exquisenseOrange]];
    [self.passwordCopiedToClipboardLabel setTextColor:[XQExquisense exquisenseOrange]];
    [self.tokenCopiedToClipboardLabel setTextColor:[XQExquisense exquisenseOrange]];
    
    [self.tokenLabel setTextAlignment:NSTextAlignmentCenter];
    [self.tokenCopiedToClipboardLabel setTextAlignment:NSTextAlignmentCenter];
    
    [self.tokenCopiedToClipboardLabel setAlpha:0];
    [self.usernameCopiedToClipboardLabel setAlpha:0];
    [self.passwordCopiedToClipboardLabel setAlpha:0];

    [self.splitViewController setDelegate:self];
    [self.scrollView setDelegate:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateView];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    UILabel *temp = [[UILabel alloc] init];
    [temp setFont:[UIFont systemFontOfSize:25]];
    [temp setText:self.passwordLabel.text];
    [temp sizeToFit];
    
    [self.scrollView setContentSize:CGSizeMake(temp.frame.size.width, 50.0)];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) self.entryDetails = nil;
    if (timer) [timer invalidate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)updateTokenCode
{
    XQTokenCode *tokenCode = [token tokenCode];
    [self.tokenProgressView setProgress:[tokenCode currentProgress]];
    [self.tokenLabel setText:[tokenCode currentCode]];
}

#pragma mark - IBActions

- (IBAction)backBarButtonPressed:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)editBarButtonPressed:(UIBarButtonItem *)sender
{
    [self performSegueWithIdentifier:TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE sender:sender];
}

- (IBAction)refreshTokenButtonPressed:(UIButton *)sender
{
    [self updateTokenCode];
    
    NSMutableDictionary *entryDetails = [self.entryDetails mutableCopy];
    NSMutableDictionary *entryToken = [[self.entryDetails objectForKey:ENTRY_TOKEN] mutableCopy];
    
    uint64_t currentCount = [[entryToken objectForKey:ENTRY_TOKEN_COUNTER] unsignedLongLongValue];
    [entryToken setObject:@(currentCount + 1) forKey:ENTRY_TOKEN_COUNTER];
    [entryDetails setObject:entryToken forKey:ENTRY_TOKEN];
    self.entryDetails = [NSDictionary dictionaryWithDictionary:entryDetails];
    
    XQExquisenseDatabase *database = [appDelegate database];
    [database setEntry:entryDetails forKey:[self.entryDetails objectForKey:ENTRY_NAME] toTable:TABLE_NAME];
    
    NSString *hashedPassword = [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
    [appDelegate saveDatabaseWithPassword:hashedPassword];
}

- (IBAction)copyUsernameButtonPressed:(UIButton *)sender
{
    if (![self.usernameLabel.text isEqualToString:@""])
    {
        [UIView animateWithDuration:1 animations:^
         {
             [self.usernameCopiedToClipboardLabel setAlpha:1];
         }];
        
        [UIView animateWithDuration:1 animations:^
         {
             [self.usernameCopiedToClipboardLabel setAlpha:0];
         }
                         completion:^(BOOL finished)
         {
             [[UIPasteboard generalPasteboard] setString:self.usernameLabel.text];
             
             [client sendData:[self.usernameLabel.text dataUsingEncoding:NSUTF8StringEncoding]];
         }];
    }
}

- (IBAction)copyPasswordButtonPressed:(UIButton *)sender
{
    if (![self.passwordLabel.text isEqualToString:@""])
    {
        [UIView animateWithDuration:1 animations:^
         {
             [self.passwordCopiedToClipboardLabel setAlpha:1];
         }];
        
        [UIView animateWithDuration:1 animations:^
         {
             [self.passwordCopiedToClipboardLabel setAlpha:0];
         }
                         completion:^(BOOL finished)
         {
             [[UIPasteboard generalPasteboard] setString:self.passwordLabel.text];
             
             [client sendData:[self.passwordLabel.text dataUsingEncoding:NSUTF8StringEncoding]];
         }];
    }
}

- (IBAction)copyTokenButtonPressed:(UIButton *)sender
{
    if (![self.tokenLabel.text isEqualToString:@""] && ![self.tokenLabel.text isEqualToString:@"--------"])
    {
        [UIView animateWithDuration:1 animations:^
         {
             [self.tokenCopiedToClipboardLabel setAlpha:1];
         }];
        
        [UIView animateWithDuration:1 animations:^
         {
             [self.tokenCopiedToClipboardLabel setAlpha:0];
         }
                         completion:^(BOOL finished)
         {
             [[UIPasteboard generalPasteboard] setString:self.tokenLabel.text];
             
             [client sendData:[self.tokenLabel.text dataUsingEncoding:NSUTF8StringEncoding]];
         }];
    }
}

- (IBAction)deleteEntryButtonPressed:(UIButton *)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Deletion"
                                                    message:@"Are you sure?"
                                                   delegate:self
                                          cancelButtonTitle:@"Yes"
                                          otherButtonTitles:@"No", nil];
    [alert show];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        XQExquisenseDatabase *database = [(XQAppDelegate *)[[UIApplication sharedApplication] delegate] database];
        
        [database removeEntryForKey:[self.entryDetails objectForKey:ENTRY_NAME] fromTable:TABLE_NAME];
        
        if (ENABLE_LOGGING) [database printCurrentState];
        
        NSString *hashedPassword = [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
        [(XQAppDelegate *)[[UIApplication sharedApplication] delegate] saveDatabaseWithPassword:hashedPassword];
        
        self.entryDetails = nil;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            [self updateView];
            [self.delegate didUpdateEntry];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - Segue Controllers

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE] &&
        [segue.destinationViewController isKindOfClass:[XQEditEntryTableViewController class]])
    {
        XQEditEntryTableViewController *editEntryTableViewController = segue.destinationViewController;
        [editEntryTableViewController setEntryDetails:self.entryDetails];
        [editEntryTableViewController setDelegate:self];
    }
    if ([segue.identifier isEqualToString:TO_EDIT_ENTRY_TABLE_VIEW_CONTROLLER_SEGUE] &&
        [segue.destinationViewController isKindOfClass:[XQEditEntryAndSecondAuthenticationTableViewController class]])
    {
        XQEditEntryAndSecondAuthenticationTableViewController *editEntryAndSecondAuthenticationTableViewController = segue.destinationViewController;
        [editEntryAndSecondAuthenticationTableViewController setEntryDetails:self.entryDetails];
        [editEntryAndSecondAuthenticationTableViewController setDelegate:self];
    }
}

#pragma mark - XQEditEntryTableViewControllerDelegate

- (void)didEditEntry:(NSDictionary *)entryDetails
{
    [self setEntryDetails:entryDetails];
    
    [self.delegate didUpdateEntry];
}

#pragma mark - XQVaultSplitViewControllerDelegate

- (void)didSelectEntry:(NSDictionary *)entryDetails
{
    [self setEntryDetails:entryDetails];
    
    if (timer) [timer invalidate];
    
    [self updateView];
}

#pragma mark - UISplitViewControllerDelegate

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return NO;
}

#pragma mark - Helper Methods

- (void)updateView
{
    if (self.entryDetails)
    {
        [self.nameLabel setText:[self.entryDetails objectForKey:ENTRY_NAME]];
        [self.usernameLabel setText:[self.entryDetails objectForKey:ENTRY_ID]];
        [self.passwordLabel setText:[self.entryDetails objectForKey:ENTRY_PASSWORD]];
        
        NSDictionary *entryToken = [self.entryDetails objectForKey:ENTRY_TOKEN];
        
        if (entryToken)
        {
            token = [[XQToken alloc] initWithTokenDetails:entryToken];
            
            if ([[entryToken objectForKey:ENTRY_TOKEN_TYPE] isEqualToString:ENTRY_TOKEN_TYPE_TOTP])
            {
                XQTokenCode *tokenCode = [token tokenCode];
                
                [self.tokenProgressView setHidden:NO];
                [self.refreshButton setHidden:YES];
                [self.tokenLabel setTextAlignment:NSTextAlignmentCenter];
                [self.tokenCopiedToClipboardLabel setTextAlignment:NSTextAlignmentCenter];
                [self.tokenProgressView setProgress:[tokenCode currentProgress]];
                
                timer = [NSTimer scheduledTimerWithTimeInterval:TIMER_INTERVAL
                                                         target:self
                                                       selector:@selector(updateTokenCode)
                                                       userInfo:nil
                                                        repeats:YES];
            }
            else if ([[entryToken objectForKey:ENTRY_TOKEN_TYPE] isEqualToString:ENTRY_TOKEN_TYPE_HOTP])
            {
                [self.tokenProgressView setHidden:YES];
                [self.refreshButton setHidden:NO];
                [self.tokenLabel setText:@"--------"];
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                {
                    [self.tokenLabel setTextAlignment:NSTextAlignmentLeft];
                    [self.tokenCopiedToClipboardLabel setTextAlignment:NSTextAlignmentLeft];
                }
            }
        }
        else
        {
            if (timer) [timer invalidate];
            
            [self.tokenProgressView setHidden:YES];
            [self.refreshButton setHidden:YES];
            [self.tokenLabel setTextAlignment:NSTextAlignmentCenter];
            [self.tokenCopiedToClipboardLabel setTextAlignment:NSTextAlignmentCenter];
            [self.tokenLabel setText:@"--------"];
        }
        
        [self.nameLabel setHidden:NO];
        [self.usernameLogo setHidden:NO];
        [self.passwordLogo setHidden:NO];
        [self.usernameCopyButton setHidden:NO];
        [self.passwordCopyButton setHidden:NO];
        [self.tokenCopyButton setHidden:NO];
        [self.deleteEntryButton setHidden:NO];
        
        NSString *pairedDevice = [client getPairedDeviceName];
        [self.connectionLabel setText:pairedDevice ? [pairedDevice uppercaseString]: @""];
        
        [self viewDidLayoutSubviews];
    }
    else
    {
        if (timer) [timer invalidate];
        
        [self.nameLabel setHidden:YES];
        [self.usernameLogo setHidden:YES];
        [self.passwordLogo setHidden:YES];
        [self.tokenProgressView setHidden:YES];
        [self.refreshButton setHidden:YES];
        [self.usernameCopyButton setHidden:YES];
        [self.passwordCopyButton setHidden:YES];
        [self.tokenCopyButton setHidden:YES];
        [self.deleteEntryButton setHidden:YES];
        [self.tokenLabel setText:@""];
        [self.usernameLabel setText:@""];
        [self.passwordLabel setText:@""];
        [self.connectionLabel setText:@""];
    }
}

@end
