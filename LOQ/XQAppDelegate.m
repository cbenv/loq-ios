//
//  XQAppDelegate.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/3/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQAppDelegate.h"
#import "XQExquisenseSecurity.h"
#import "XQSignUpViewController.h"
#import "XQLogOnViewController.h"

#define SIGN_UP_VIEW_CONTROLLER @"signUpViewController"
#define LOG_ON_VIEW_CONTROLLER @"logOnViewController"

#define ADD_SAMPLE_ENTRIES YES

@implementation XQAppDelegate

@synthesize database;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self.window setBackgroundColor:[UIColor whiteColor]];
    [self.window makeKeyAndVisible];
    
    [XQExquisense applyStyle];
    
    self.database = [[XQExquisenseDatabase alloc] init];
    [self.database setTableForKey:TABLE_NAME];
    
    self.client = [[XQExquisenseBonjourClient alloc] initWithServiceType:@"clipboard"];
    // self.client = [[XQExquisenseBonjourClient alloc] initWithServiceType:@"clipboard" inDomain:@"local."];
    
    if (ADD_SAMPLE_ENTRIES) [self addSampleEntries];
    
    if (ENABLE_LOGGING) [self.database printCurrentState];
    
    UIStoryboard *storyboard;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        storyboard = [UIStoryboard storyboardWithName:@"iPad" bundle:nil];
    }
    else
    {
        storyboard = [UIStoryboard storyboardWithName:@"iPhone" bundle:nil];
    }
    
    UIViewController *root;
    
    NSString *masterCredentials = [[NSUserDefaults standardUserDefaults] objectForKey:MASTER_CREDENTIALS_KEY];
    
    if (!masterCredentials)
    {
        root = [storyboard instantiateViewControllerWithIdentifier:SIGN_UP_VIEW_CONTROLLER];
    }
    else
    {
        root = [storyboard instantiateViewControllerWithIdentifier:LOG_ON_VIEW_CONTROLLER];
    }

    self.window.rootViewController = root;
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [self.client stop];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Database

- (void)saveDatabaseWithPassword:(NSString *)password
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.database];
    
    if (password)
    {
        data = [XQExquisenseSecurity AESEncryptOfData:data withKey:password];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:DATABASE_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if (ENABLE_LOGGING) NSLog(@"Saved database of size %lu bytes", (unsigned long)[data length]);
}

- (void)loadDatabaseWithPassword:(NSString *)password
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:DATABASE_KEY];
    
    if (password)
    {
        data = [XQExquisenseSecurity AESDecryptOfData:data withKey:password];
    }
    
    self.database = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (ENABLE_LOGGING) NSLog(@"Loaded database of size %lu bytes", (unsigned long)[data length]);
}

#pragma mark - Helper Methods

- (void)addSampleEntries
{
    NSMutableDictionary *entryTOTP = [NSMutableDictionary dictionary];
    NSMutableDictionary *entryTokenTOTP = [NSMutableDictionary dictionary];
    
    [entryTOTP setObject:@"TOTP Sample" forKey:ENTRY_NAME];
    [entryTOTP setObject:@"username" forKey:ENTRY_ID];
    [entryTOTP setObject:@"password" forKey:ENTRY_PASSWORD];
    [entryTokenTOTP setObject:ENTRY_TOKEN_TYPE_TOTP forKey:ENTRY_TOKEN_TYPE];
    [entryTokenTOTP setObject:@"TOTP" forKey:ENTRY_TOKEN_SECRET];
    [entryTokenTOTP setObject:@(20) forKey:ENTRY_TOKEN_INTERVAL];
    [entryTokenTOTP setObject:@(8) forKey:ENTRY_TOKEN_DIGITS];
    [entryTokenTOTP setObject:ENTRY_TOKEN_ALGORITHM_SHA512 forKey:ENTRY_TOKEN_ALGORITHM];
    [entryTOTP setObject:entryTokenTOTP forKey:ENTRY_TOKEN];
    
    [self.database setEntry:entryTOTP forKey:@"TOTP Sample" toTable:TABLE_NAME];
    
    NSMutableDictionary *entryHOTP = [NSMutableDictionary dictionary];
    NSMutableDictionary *entryTokenHOTP = [NSMutableDictionary dictionary];
    
    [entryHOTP setObject:@"HOTP Sample" forKey:ENTRY_NAME];
    [entryHOTP setObject:@"username" forKey:ENTRY_ID];
    [entryHOTP setObject:@"password" forKey:ENTRY_PASSWORD];
    [entryTokenHOTP setObject:ENTRY_TOKEN_TYPE_HOTP forKey:ENTRY_TOKEN_TYPE];
    [entryTokenHOTP setObject:@"HOTP" forKey:ENTRY_TOKEN_SECRET];
    [entryTokenHOTP setObject:@(0) forKey:ENTRY_TOKEN_INTERVAL];
    [entryTokenHOTP setObject:@(8) forKey:ENTRY_TOKEN_DIGITS];
    [entryTokenHOTP setObject:ENTRY_TOKEN_ALGORITHM_SHA512 forKey:ENTRY_TOKEN_ALGORITHM];
    [entryHOTP setObject:entryTokenHOTP forKey:ENTRY_TOKEN];
    [self.database setEntry:entryHOTP forKey:@"HOTP Sample" toTable:TABLE_NAME];
}

@end
