//
//  XQExquisenseBonjourClient.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/22/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XQExquisenseBonjourClientDelegate <NSObject>

@optional

- (void)didRefreshBroadcastingDevices:(NSArray *)broadCastingDevices;
- (void)didReceiveData:(NSData *)data;
- (void)didDisconnect;
- (void)didConnect;

@end

@interface XQExquisenseBonjourClient : NSObject

@property (weak, nonatomic) id <XQExquisenseBonjourClientDelegate> delegate;

- (id)init;
- (id)initWithServiceType:(NSString *)type;
- (id)initWithServiceType:(NSString *)type inDomain:(NSString *)domain;
- (void)startBrowsing;
- (void)stop;

- (void)setNetService:(NSNetService *)service;
- (NSString *)getPairedDeviceName;

- (void)sendData:(NSData *)data;

@end
