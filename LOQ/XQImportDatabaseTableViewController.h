//
//  XQImportDatabaseTableViewController.h
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/17/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQExquisenseTableViewController.h"

@interface XQImportDatabaseTableViewController : XQExquisenseTableViewController

@end
