//
//  XQConfigureNetworkingTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/18/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQConfigureNetworkingTableViewController.h"
#import "XQAppDelegate.h"
#import "XQExquisenseBonjourClient.h"

#define CELL_IDENTIFIER @"deviceCell"

@interface XQConfigureNetworkingTableViewController () <XQExquisenseBonjourClientDelegate>
{
    XQAppDelegate *appDelegate;
    
    XQExquisenseBonjourClient *client;
    NSArray *devices;
}

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender;

@end

@implementation XQConfigureNetworkingTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    
    client = [appDelegate client];
    [client setDelegate:self];
    [client startBrowsing];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [devices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    NSNetService *service = [devices objectAtIndex:indexPath.row];
    [cell.textLabel setText:service.name];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSNetService *netService = [devices objectAtIndex:indexPath.row];
    [client setNetService:netService];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

#pragma mark - XQExquisenseBonjourClientDelegate

- (void)didRefreshBroadcastingDevices:(NSArray *)broadCastingDevices
{
    devices = broadCastingDevices;
    
    [self.tableView reloadData];
}

- (void)didConnect
{
    
}

- (void)didDisconnect
{
    
}

- (void)didReceiveData:(NSData *)data
{
    
}

@end
