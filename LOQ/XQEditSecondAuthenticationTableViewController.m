//
//  XQEditSecondAuthenticationTableViewController.m
//  LOQ
//
//  Created by Christoforus Juan Benvenuto on 7/5/14.
//  Copyright (c) 2014 Exquisense. All rights reserved.
//

#import "XQEditSecondAuthenticationTableViewController.h"

@interface XQEditSecondAuthenticationTableViewController () <UITextFieldDelegate>
{
    NSMutableDictionary *entryTokenTemp;
}

@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneBarButton;

@property (strong, nonatomic) IBOutlet UITableViewCell *tokenTypeTOTPCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *tokenTypeHOTPCell;

@property (strong, nonatomic) IBOutlet UITextField *tokenIssuerTextField;
@property (strong, nonatomic) IBOutlet UITextField *tokenIDTextField;
@property (strong, nonatomic) IBOutlet UISegmentedControl *tokenSecretType;
@property (strong, nonatomic) IBOutlet UITextField *tokenSecretTextField;
@property (strong, nonatomic) IBOutlet UILabel *tokenIntervalCounterLabel;
@property (strong, nonatomic) IBOutlet UILabel *tokenDigitsLabel;
@property (strong, nonatomic) IBOutlet UILabel *intervalCounterLabel;
@property (strong, nonatomic) IBOutlet UIStepper *intervalCounterStepper;
@property (strong, nonatomic) IBOutlet UIStepper *digitsStepper;

@property (strong, nonatomic) IBOutlet UITableViewCell *tokenAlgorithmMD5Cell;
@property (strong, nonatomic) IBOutlet UITableViewCell *tokenAlgorithmSHA1Cell;
@property (strong, nonatomic) IBOutlet UITableViewCell *tokenAlgorithmSHA256Cell;
@property (strong, nonatomic) IBOutlet UITableViewCell *tokenAlgorithmSHA512Cell;

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender;
- (IBAction)doneBarButtonPressed:(UIBarButtonItem *)sender;

- (IBAction)tokenSecretTypeChanged:(UISegmentedControl *)sender;
- (IBAction)intervalCounterStepperChanged:(UIStepper *)sender;
- (IBAction)digitStepperChanged:(UIStepper *)sender;

- (IBAction)requiredFieldFilled:(id)sender;

@end

@implementation XQEditSecondAuthenticationTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tokenIssuerTextField setDelegate:self];
    [self.tokenIDTextField setDelegate:self];
    [self.tokenSecretTextField setDelegate:self];
    [self.tokenIssuerTextField setReturnKeyType:UIReturnKeyNext];
    [self.tokenIDTextField setReturnKeyType:UIReturnKeyNext];
    [self.tokenSecretTextField setReturnKeyType:UIReturnKeyDone];
    [self.doneBarButton setEnabled:NO];
    [self.tokenSecretType setSelectedSegmentIndex:1];
    [self.tokenSecretTextField setPlaceholder:@"32 base-32 characters"];
    [self.digitsStepper setMinimumValue:ENTRY_TOKEN_DIGITS_MINIMUM];
    [self.digitsStepper setMaximumValue:ENTRY_TOKEN_DIGITS_MAXIMUM];
    
    if (self.entryToken)
    {
        entryTokenTemp = [self.entryToken mutableCopy];
        
        NSString *tokenType = [entryTokenTemp valueForKey:ENTRY_TOKEN_TYPE];
        NSString *tokenAlgorithm = [entryTokenTemp valueForKey:ENTRY_TOKEN_ALGORITHM];
        NSString *tokenSecretType = [entryTokenTemp valueForKey:ENTRY_TOKEN_SECRET_TYPE];
        
        if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_TOTP])
        {
            [self.tokenTypeTOTPCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            [self.intervalCounterLabel setText:@"Interval"];
            
            [self.intervalCounterStepper setValue:[[entryTokenTemp valueForKey:ENTRY_TOKEN_INTERVAL] intValue]];
            [self.intervalCounterStepper setMaximumValue:ENTRY_TOKEN_INTERVAL_MAXIMUM];
            [self.intervalCounterStepper setMinimumValue:ENTRY_TOKEN_INTERVAL_MINIMUM];
        }
        else if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_HOTP])
        {
            [self.tokenTypeHOTPCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            [self.intervalCounterLabel setText:@"Counter"];
            
            [self.intervalCounterStepper setValue:[[entryTokenTemp valueForKey:ENTRY_TOKEN_COUNTER] intValue]];
            [self.intervalCounterStepper setMaximumValue:ENTRY_TOKEN_COUNTER_MAXIMUM];
            [self.intervalCounterStepper setMinimumValue:ENTRY_TOKEN_COUNTER_MINIMUM];
        }
        
        if ([tokenAlgorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_MD5])
        {
            [self.tokenAlgorithmMD5Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else if ([tokenAlgorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA1])
        {
            [self.tokenAlgorithmSHA1Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else if ([tokenAlgorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA256])
        {
            [self.tokenAlgorithmSHA256Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else if ([tokenAlgorithm isEqualToString:ENTRY_TOKEN_ALGORITHM_SHA512])
        {
            [self.tokenAlgorithmSHA512Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        
        if ([tokenSecretType isEqualToString:ENTRY_TOKEN_SECRET_TYPE_HEX])
        {
            [self.tokenSecretType setSelectedSegmentIndex:0];
            [self.tokenSecretTextField setPlaceholder:@"40 hexadecimal characters"];
        }
        else if ([tokenSecretType isEqualToString:ENTRY_TOKEN_SECRET_TYPE_BASE32])
        {
            [self.tokenSecretType setSelectedSegmentIndex:1];
            [self.tokenSecretTextField setPlaceholder:@"32 base-32 characters"];
        }
        
        [self.tokenIssuerTextField setText:[entryTokenTemp valueForKey:ENTRY_TOKEN_ISSUER]];
        [self.tokenIDTextField setText:[entryTokenTemp valueForKey:ENTRY_TOKEN_ID]];
        [self.tokenSecretTextField setText:[entryTokenTemp valueForKey:ENTRY_TOKEN_SECRET]];
        [self.digitsStepper setValue:[[entryTokenTemp valueForKey:ENTRY_TOKEN_DIGITS] intValue]];
        
        [self intervalCounterStepperChanged:self.intervalCounterStepper];
        [self digitStepperChanged:self.digitsStepper];
    }
    else
    {
        entryTokenTemp = [[NSMutableDictionary alloc] init];
        
        [self.intervalCounterLabel setText:@"Interval"];
        [self.intervalCounterStepper setValue:ENTRY_TOKEN_INTERVAL_DEFAULT];
        [self.digitsStepper setValue:ENTRY_TOKEN_DIGITS_DEFAULT];
        [self intervalCounterStepperChanged:self.intervalCounterStepper];
        [self digitStepperChanged:self.digitsStepper];
    }
    
    [self requiredFieldFilled:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions

- (IBAction)cancelBarButtonPressed:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneBarButtonPressed:(UIBarButtonItem *)sender
{
    [entryTokenTemp setValue:self.tokenIssuerTextField.text forKey:ENTRY_TOKEN_ISSUER];
    [entryTokenTemp setValue:self.tokenIDTextField.text forKey:ENTRY_TOKEN_ID];
    [entryTokenTemp setValue:self.tokenSecretTextField.text forKey:ENTRY_TOKEN_SECRET];
    [entryTokenTemp setValue:@(self.digitsStepper.value) forKey:ENTRY_TOKEN_DIGITS];
    
    NSString *tokenType = [entryTokenTemp valueForKey:ENTRY_TOKEN_TYPE];
    if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_TOTP])
    {
        [entryTokenTemp setValue:@(self.intervalCounterStepper.value) forKey:ENTRY_TOKEN_INTERVAL];
        [entryTokenTemp setValue:nil forKey:ENTRY_TOKEN_COUNTER];
    }
    else if ([tokenType isEqualToString:ENTRY_TOKEN_TYPE_HOTP])
    {
        [entryTokenTemp setValue:@(self.intervalCounterStepper.value) forKey:ENTRY_TOKEN_COUNTER];
        [entryTokenTemp setValue:nil forKey:ENTRY_TOKEN_INTERVAL];
    }
    
    if ([self.tokenSecretType selectedSegmentIndex] == 0)
    {
        [entryTokenTemp setValue:ENTRY_TOKEN_SECRET_TYPE_HEX forKey:ENTRY_TOKEN_SECRET_TYPE];
    }
    else // if ([self.tokenSecretType selectedSegmentIndex] == 1)
    {
        [entryTokenTemp setValue:ENTRY_TOKEN_SECRET_TYPE_BASE32 forKey:ENTRY_TOKEN_SECRET_TYPE];
    }
    
    [self.delegate didConfigure:entryTokenTemp];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)tokenSecretTypeChanged:(UISegmentedControl *)sender
{
    [self.tokenSecretTextField setText:@""];
    
    if ([self.tokenSecretType selectedSegmentIndex] == 0)
    {
        [self.tokenSecretTextField setPlaceholder:@"40 hexadecimal characters"];
    }
    else if ([self.tokenSecretType selectedSegmentIndex] == 1)
    {
        [self.tokenSecretTextField setPlaceholder:@"32 base-32 characters"];
    }
}

- (IBAction)intervalCounterStepperChanged:(UIStepper *)sender
{
    [self.tokenIntervalCounterLabel setText:[NSString stringWithFormat:@"%i", (int) sender.value]];
}

- (IBAction)digitStepperChanged:(UIStepper *)sender
{
    [self.tokenDigitsLabel setText:[NSString stringWithFormat:@"%i", (int) sender.value]];
}

- (IBAction)requiredFieldFilled:(id)sender
{
    if ([self.tokenSecretType selectedSegmentIndex] == 0 && [self.tokenSecretTextField.text length] == 40)
    {
        [self.doneBarButton setEnabled:YES];
    }
    else if ([self.tokenSecretType selectedSegmentIndex] == 1 && [self.tokenSecretTextField.text length] == 32)
    {
        [self.doneBarButton setEnabled:YES];
    }
    else
    {
        [self.doneBarButton setEnabled:NO];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            [self.tokenTypeHOTPCell setAccessoryType:UITableViewCellAccessoryNone];
            [self.tokenTypeTOTPCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [self.intervalCounterLabel setText:@"Interval"];
            [self.intervalCounterStepper setMaximumValue:ENTRY_TOKEN_INTERVAL_MAXIMUM];
            [self.intervalCounterStepper setMinimumValue:ENTRY_TOKEN_INTERVAL_MINIMUM];
            [self.intervalCounterStepper setValue:ENTRY_TOKEN_INTERVAL_DEFAULT];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_TYPE_TOTP forKey:ENTRY_TOKEN_TYPE];
        }
        else if (indexPath.row == 1)
        {
            [self.tokenTypeTOTPCell setAccessoryType:UITableViewCellAccessoryNone];
            [self.tokenTypeHOTPCell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [self.intervalCounterLabel setText:@"Counter"];
            [self.intervalCounterStepper setMaximumValue:ENTRY_TOKEN_COUNTER_MAXIMUM];
            [self.intervalCounterStepper setMinimumValue:ENTRY_TOKEN_COUNTER_MINIMUM];
            [self.intervalCounterStepper setValue:ENTRY_TOKEN_COUNTER_DEFAULT];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_TYPE_HOTP forKey:ENTRY_TOKEN_TYPE];
        }
        
        [self intervalCounterStepperChanged:self.intervalCounterStepper];
    }
    else if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            [self.tokenIssuerTextField becomeFirstResponder];
        }
        else if (indexPath.row == 1)
        {
            [self.tokenIDTextField becomeFirstResponder];
        }
        else if (indexPath.row == 2)
        {
            [self.tokenSecretTextField becomeFirstResponder];
        }
    }
    else if (indexPath.section == 2)
    {
        [self.tokenAlgorithmMD5Cell setAccessoryType:UITableViewCellAccessoryNone];
        [self.tokenAlgorithmSHA1Cell setAccessoryType:UITableViewCellAccessoryNone];
        [self.tokenAlgorithmSHA256Cell setAccessoryType:UITableViewCellAccessoryNone];
        [self.tokenAlgorithmSHA512Cell setAccessoryType:UITableViewCellAccessoryNone];
        
        if (indexPath.row == 0)
        {
            [self.tokenAlgorithmMD5Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_ALGORITHM_MD5 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if (indexPath.row == 1)
        {
            [self.tokenAlgorithmSHA1Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_ALGORITHM_SHA1 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if (indexPath.row == 2)
        {
            [self.tokenAlgorithmSHA256Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_ALGORITHM_SHA256 forKey:ENTRY_TOKEN_ALGORITHM];
        }
        else if (indexPath.row == 3)
        {
            [self.tokenAlgorithmSHA512Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            
            [entryTokenTemp setValue:ENTRY_TOKEN_ALGORITHM_SHA512 forKey:ENTRY_TOKEN_ALGORITHM];
        }
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.tokenIssuerTextField])
    {
        [self.tokenIDTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.tokenIDTextField])
    {
        [self.tokenSecretTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.tokenSecretTextField])
    {
        [self.tokenSecretTextField resignFirstResponder];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:self.tokenSecretTextField])
    {
        NSString* newstr = [textField.text stringByReplacingCharactersInRange:range withString:string];
        const char *newbuf = [newstr UTF8String];
        
        BOOL unpadded = NO;
        for (unsigned long i = [newstr length]; i > 0; i--)
        {
            if (!unpadded)
            {
                if (newbuf[i - 1] == '=')
                {
                    continue;
                }
                else
                {
                    unpadded = YES;
                }
            }
            
            if ([self.tokenSecretType selectedSegmentIndex] == 0)
            {
                if (!strchr("abdefABCDEF0123456789", newbuf[i - 1]))
                {
                    return NO;
                }
            }
            else // if ([self.tokenSecretType selectedSegmentIndex] == 1)
            {
                if (!strchr("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ234567", newbuf[i - 1]))
                {
                    return NO;
                }
            }
        }
    }
    return YES;
}

@end
